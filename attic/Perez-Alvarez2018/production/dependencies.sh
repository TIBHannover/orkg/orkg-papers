#!/bin/bash

Rscript -e 'remotes::install_gitlab("TIBHannover/orkg/orkg-r")'
Rscript -e 'install.packages(c("MuMIn", "visreg", "effects", "ggpubr"), repos="http://cran.us.r-project.org")'