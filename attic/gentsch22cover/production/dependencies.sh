#!/bin/bash

Rscript -e 'remotes::install_gitlab("TIBHannover/orkg/orkg-r")'
Rscript -e 'install.packages(c("emmeans", "lmerTest", "pairwiseCI", "multcompView", "ggpubr", "PerformanceAnalytics", "lavaan", "tidySEM", "ggbiplot"), repos="http://cran.us.r-project.org")'
Rscript -e 'install.packages("lme4", type = "source", repos="http://cran.us.r-project.org")'