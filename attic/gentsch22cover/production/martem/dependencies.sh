#!/bin/bash

Rscript -e 'devtools::install_github("OlgaLezhnina/martem")'
Rscript -e 'install.packages(c("sets", "tidyverse"), repos="http://cran.us.r-project.org")'