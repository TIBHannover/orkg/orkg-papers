from datetime import datetime
from orkg import ORKG, Hosts, OID
import json
import time
import requests
import pandas as pd
import os
import sys
import shutil
import statistics
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

#set up the connection to ORKG
orkg = ORKG(host="https://incubating.orkg.org", creds=('username', 'password'))
orkg.templates.materialize_template("R830127")
tp = orkg.templates

#Save the csv data as a pandas dataframe
inputFile = sys.argv[1]
df = pd.read_csv(inputFile, sep=";")
df2 = df.drop_duplicates(subset=['DOI'])

#some metadata cannot be fetched, this is added manually
metadata_df = pd.read_csv("missing_metadata.csv",sep=";")

#save IDs
res_ids = {}
city_ids = {}
country_ids = {}
region_ids = {}
sosa_observations = {}
paper_ids = {}
unknown_papers = []
com_ids = {}
#if the resources have already been created, then load from the JSON 
if os.path.isfile("papers_ids.json"):
    with open("papers_ids.json", "r") as f:
        paper_ids = json.load(f)
if os.path.isfile("resource_ids.json"):
    with open("resource_ids.json","r") as f:
        res_ids = json.load(f)
if os.path.isfile("city_ids.json"):
    with open("city_ids.json","r") as f:
        city_ids = json.load(f)
if os.path.isfile("country_ids.json"):
    with open("country_ids.json","r") as f:
        country_ids = json.load(f)
if os.path.isfile("region_ids.json"):
    with open("region_ids.json","r") as f:
        region_ids = json.load(f)
if os.path.isfile("comparison_ids.json"):
    with open("comparison_ids.json","r") as f:
        com_ids = json.load(f)

countries = list(df["Country"].unique())
cities = list(df["City"].unique())
regions = list(df["GeographicalRegion"].unique())
column_names = list(df.columns)
key_OID_dict = {
    "City": "P66035",
    "Country": "P15249",
    "GeographicalRegion":"P108000"
}

def save_resources_to_JSON():
    '''
    The resource ids are saved in a file, so they won't be added to ORKG, if the script is running again
    '''
    with open("resource_ids.json","w") as f:
        json.dump(res_ids,f)
    with open("city_ids.json","w") as f:
        json.dump(city_ids,f)
    with open("country_ids.json","w") as f:
        json.dump(country_ids,f)
    with open("region_ids.json","w") as f:
        json.dump(region_ids,f)

def get_crossref_data(path:str, doi: str):
    """
    Originally from Quentin Münch with some changes to fit this implementation.
    This function calls the crossref API to retrieve metadata for a given doi. It saves it to a JSON-File
    :param doi: the doi that should be queried
    :param index: index of current paper in dataframe
    """

    if not doi:
        return {}

    crossref_url = 'https://api.crossref.org/works/' + str(doi)

    try:
        response = requests.get(crossref_url)

    except Exception as e:
        print(e)
        print("got connection error in crossref, trying again...")
        time.sleep(60)
        try:
            response = requests.get(crossref_url)
        except Exception as e:
            print(e)
            print("got connection error in crossref, leaving out")

    if response.ok:
        with open(path+'/metadata.json','w') as f:
            json.dump(json.loads(response.content), f)
    
def get_location_ids(): 
    '''
    Use ORKG resource lookup, to see if a location already exists, if not create new resource
    Saves the ids for city, country and geographical region in a seperate file
    '''
    for city in cities:
        if city in city_ids:
            continue
        city_id = ""
        res = orkg.resources.get(q=city,exact=True,size=30,sort='label',desc=True).content
        for r in res:
            city_id = r["id"]
            city_ids[city] = city_id    
            break
        if city_id == "":
            city_ids[city] = orkg.resources.add(label=city,classes=['DCLocation']).content["id"]
    for country in countries:
        if country in country_ids:
            continue
        country_id = ""
        res = orkg.resources.get(q=country,exact=True,size=30,sort='label',desc=True).content
        for r in res:
            country_id = r["id"]
            country_ids[country] = country_id
            break
        if country_id == "":
            country_ids[country] = orkg.resources.add(label=country,classes=['DCLocation']).content["id"]
    for region in regions:
        if region in region_ids:
            continue
        region_id = ""
        res = orkg.resources.get(q=region,exact=True,size=30,sort='label',desc=True).content
        for r in res:
            region_id = r["id"]
            region_ids[region] = region_id
            break
        if region_id == "":
            region_ids[region] = orkg.resources.add(label=region,classes=['DCLocation']).content["id"]

def add_resources():
    '''
    creates the resources in ORKG that do not already exist.
    '''
    #use qudt for units
    ugm3_id = "qudt:MicroGM-PER-M3"
    mgm3_id = "qudt:MilliGM-PER-M3"
    percent = "R48045"
    concentration = "R147322"   
    res_ids["NO2"] = "R140501"
    res_ids["ugm3"] = ugm3_id
    res_ids["mgm3"] = mgm3_id
    res_ids["percent"] = percent
    res_ids["concentration"] = concentration
    res_ids["AOD"] = "R144244"
    
    if not (orkg.resources.exists(id= ugm3_id)):
        orkg.resources.add(id=ugm3_id, label="micrograms per cubic metre")
        literal = orkg.literals.add(label=f"https://qudt.org/vocab/unit/{ugm3_id.split(':')[1]}")
        orkg.statements.add(subject_id=ugm3_id, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("ugm3")
        
    if not (orkg.resources.exists(id= mgm3_id)):
        orkg.resources.add(id=mgm3_id, label="milligrams per cubic metre")
        literal = orkg.literals.add(label=f"https://qudt.org/vocab/unit/{mgm3_id.split(':')[1]}")
        orkg.statements.add(subject_id=mgm3_id, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("mgm3")
    
    
    #use wikidata for the polutants
    NOX = "wikidata:Q424418"
    res_ids["NOX"] = NOX
    if not (orkg.resources.exists(id=NOX)):
        orkg.resources.add(id=NOX, label="Nitrogen Oxide")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{NOX.split(':')[1]}")
        orkg.statements.add(subject_id=NOX, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("NOX")

    CO = "wikidata:Q2025"
    res_ids["CO"] = CO
    if not (orkg.resources.exists(id=CO)):
        orkg.resources.add(id=CO, label="Carbon Monoxide")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{CO.split(':')[1]}")
        orkg.statements.add(subject_id=CO, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("CO")
    
    PM25 = "wikidata:Q48035814"
    res_ids["PM25"] = PM25
    if not (orkg.resources.exists(id=PM25)): 
        orkg.resources.add(id=PM25, label="PM 2.5")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{PM25.split(':')[1]}")
        orkg.statements.add(subject_id=PM25, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("PM25")

    PM10 = "wikidata:Q48035980"
    res_ids["PM10"] = PM10
    if not (orkg.resources.exists(id=PM10)):
        orkg.resources.add(id=PM10, label="PM 10")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{PM10.split(':')[1]}")
        orkg.statements.add(subject_id=PM10, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("PM10")

    O3 = "wikidata:Q36933"
    res_ids["O3"] = O3
    if not (orkg.resources.exists(id=O3)):
        orkg.resources.add(id=O3, label="Ozone")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{O3.split(':')[1]}")
        orkg.statements.add(subject_id=O3, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("O3")

    SO2 =  "wikidata:Q5282"
    res_ids["SO2"] = SO2
    if not (orkg.resources.exists(id=SO2)):
        orkg.resources.add(id=SO2, label="Sulfur Dioxide")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{SO2.split(':')[1]}")
        orkg.statements.add(subject_id=SO2, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("SO2")
    
    NH3 = "wikidata:Q4087"
    res_ids["NH3"] = NH3
    if not (orkg.resources.exists(id=NH3)):
        orkg.resources.add(id=NH3, label="Ammonia")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{NH3.split(':')[1]}")
        orkg.statements.add(subject_id=NH3, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("NH3")

    NMVOC = "wikidata:Q16963326"
    res_ids["NMVOCS"] = NMVOC
    if not (orkg.resources.exists(id=NMVOC)):
        orkg.resources.add(id=NMVOC, label="Non-Methane Volatile Organic Compound")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{NMVOC.split(':')[1]}")
        orkg.statements.add(subject_id=NMVOC, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("NMVOC")

    BC = "wikidata:Q3233590"
    res_ids["BC"] = BC
    if not (orkg.resources.exists(id=BC)):
        orkg.resources.add(id=BC, label="Black Carbon")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{BC.split(':')[1]}")
        orkg.statements.add(subject_id=BC, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("BC")

    AQI = "wikidata:Q2364111"
    res_ids["AQI"] = AQI
    if not (orkg.resources.exists(id=AQI)):
        orkg.resources.add(id=AQI, label="Air Quality Index")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{AQI.split(':')[1]}")
        orkg.statements.add(subject_id=AQI, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("AQI")

    NO3 = "wikidata:Q182168"
    res_ids["NO3"] = NO3
    if not (orkg.resources.exists(id=NO3)):
        orkg.resources.add(id=NO3, label="Nitrate Ion")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{NO3.split(':')[1]}")
        orkg.statements.add(subject_id=NO3, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("NO3")
    
    SO4 = "wikidata:Q172290"
    res_ids["SO4"] = SO4
    if not (orkg.resources.exists(id=SO4)):
        orkg.resources.add(id=SO4, label="Sulfate Ion")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{SO4.split(':')[1]}")
        orkg.statements.add(subject_id=SO4, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("SO4")

    OM = "wikidata:Q1783121"
    res_ids["OM"] = OM
    if not (orkg.resources.exists(id=OM)):
        orkg.resources.add(id=OM, label="Organic Matter")
        literal = orkg.literals.add(label=f"https://www.wikidata.org/wiki/{OM.split(':')[1]}")
        orkg.statements.add(subject_id=OM, predicate_id="SAME_AS", object_id=literal.content["id"])
        print("OM")
    
    

    # these neither exist in ORKG nor in wikidata
    if not "BCFF" in res_ids:
        res_ids["BCFF"] = orkg.resources.add(label="Black Carbon from fossil-fuel combustion").content["id"]
        print("BCFF")
    if not "BCWB" in res_ids:
        res_ids["BCWB"] = orkg.resources.add(label="Black Carbon from wood burning").content["id"]
        print("BCWB")
    if not "PM1" in res_ids:
        res_ids["PM1"] = orkg.resources.add(label="PM 1").content["id"]
        print("PM1")
    if not "BBOA" in res_ids:
        res_ids["BBOA"] = orkg.resources.add(label="Biomass Burning Organic Aerosol").content["id"]
        print("BBOA")
    if not "HOA" in res_ids:
        res_ids["HOA"] = orkg.resources.add(label="Hydrocarbon-like Organic Aerosols").content["id"]
        print("HOA")
    if not "OOA" in res_ids:
        res_ids["OOA"] = orkg.resources.add(label="oxygenate Organic Aerosols").content["id"]
        print("OOA")
    
    get_location_ids()
    save_resources_to_JSON()

def create_contribution(path: str,row, column_names, con_number):
    '''
    this function collects all necessary data for a contribution 
    and saves it to a JSON-file according to the template
    :param path: the path where the JSON-file will be saved 
    :param row: current row in the dataframe
    :param column_names: list of the names of the columns in the dataframe
    :param con_number: the number of the current contribution regarding to one paper
    '''
    #save which pollutants occurs in the paper
    all_polutants = ["NO2","NOX", "CO","PM25","PM10","O3","SO2","NH3","NMVOCS","AOD","BC","AQI","BCFF","BCWB","NO3","SO4","OM","PM1","BBOA","HOA","OOA"]
    occuring_polutants = {}
    sIndex = None
    additional = ""
    #collect all the data that is not null
    for index, element in enumerate(row):
        column_name = column_names[index]
        if str(element) == "nan":
            if column_name == "AdditionalDetails":
                additional = None
            elif column_name == "StringencyIndex":
                sIndex = None
            continue
        if column_name == "StudyStartDate":
            start = element
        elif column_name == "StudyEndDate":
            end = element
        elif column_name == "DOI":
            doi = element
        elif column_name == "City":
            city = element
        elif column_name == "Country":
            country = element
        elif column_name == "GeographicalRegion":
            region = element
        elif column_name == "AdditionalDetails":
            additional = str(element)
        elif column_name == "methods":
            method = element
        elif column_name == "platform":
            if element == "both":
                platform = ["Ground-based","Satellites"]
            else:
                platform = element
        elif column_name == "StringencyIndex":
            sIndex = element
        for p in all_polutants:
            if p in column_name:
                if p not in occuring_polutants:
                    occuring_polutants[p] = ""
                if "_er" in column_name:
                    continue
                if "prcnt_change" in column_name and "prcnt_change_er" not in column_name:
                    occuring_polutants[p] += "percent:"+str(element)+ " "
                elif "Reference_avg" in column_name and str(element) != "nan" :
                    if "ugm3" in column_name:
                        occuring_polutants[p] += "unit:ugm3 ref:"+str(element) +" "
                    elif "mgm3" in column_name:
                        occuring_polutants[p] += "unit:mgm3 ref:"+str(element)+ " "
                    else:
                        occuring_polutants[p] += "ref:"+str(element) +" "
                elif "Lockdown_avg" in column_name and str(element) != "nan" :
                    if "ugm3" in column_name:
                        occuring_polutants[p] += " unit:ugm3 lock:"+str(element) + " "
                    elif "mgm3" in column_name:
                        occuring_polutants[p] += " unit:mgm3 lock:"+str(element) + " "
                    else:
                        occuring_polutants[p] += "lock:"+str(element)
            
    #add data to JSON-File
    city_oid = city_ids[city]
    country_oid = country_ids[country]
    region_oid = region_ids[region]
    if not ("other" in str(city)  or "urban" in str(city)):
        name = city + " " + str(start)
        common_name = city
    elif not  "other" in str(country):
        name = country + " " + str(start)
        common_name = country
    else:
        name = region + " " + str(start)
        common_name = region
    #make a list of all measurements
    observations = []
    
    for key in occuring_polutants:
        data = occuring_polutants[key].split(" ")
        unit = None
        lock_value = None
        ref_value = None
        percent_value = None
        for e in data:
            if "unit" in e:
                if e.split(":")[1] == "mgm3":
                    unit = res_ids["mgm3"]
                    unit_name = "mgm3"
                else:
                    unit = res_ids["ugm3"]
                    unit_name = "ugm3"
            if "ref" in e:
                ref_value = float(e.split(":")[1])
            elif "lock" in e:
                lock_value = float(e.split(":")[1])
            elif "percent" in e:
                percent_value = float(e.split(":")[1])
        if unit != None:
            o = tp.sosaobservation(
                label = key,
                has_result = tp.airqualityresult(
                    label = "ref:"+ str(ref_value) + unit_name + ",lockdown:" + str(lock_value) + unit_name + ", change:" +str(percent_value) + "%",
                    has_lockdown_value = tp.quantity_value(
                        qudtunit = OID(unit), 
                        qudtnumericvalue = lock_value,
                    ),
                    has_reference_value = tp.quantity_value(
                        qudtunit = OID(unit), 
                        qudtnumericvalue = ref_value,
                    ),
                    has_relative_change = tp.quantity_value(
                        qudtunit = OID(res_ids["percent"]), 
                        qudtnumericvalue = percent_value,
                    ),
                ),
                observed_property = OID(res_ids["concentration"]),
                has_feature_of_interest = OID(res_ids[key]), 
            )
        else:
            if lock_value == None:
                o = tp.sosaobservation(
                    label = key,
                    has_result = tp.airqualityresult(
                        label = str(percent_value) + " Percent",
                        has_relative_change = tp.quantity_value(
                            qudtunit = OID(res_ids["percent"]), 
                            qudtnumericvalue = percent_value,
                        ),
                    ),
                    has_feature_of_interest = OID(res_ids[key]),
                    observed_property = OID(res_ids["concentration"]),
                )
            else:
                o = tp.sosaobservation(
                    label = key,
                    has_result = tp.airqualityresult(
                        label = "ref:"+ str(ref_value) + ",lockdown:" + str(lock_value)+ ", change:" +str(percent_value) + " Percent",
                        has_lockdown_value = lock_value,
                        has_reference_value = ref_value,
                        has_relative_change = tp.quantity_value(
                            qudtunit = OID(res_ids["percent"]), 
                            qudtnumericvalue = percent_value,
                        ),
                    ),
                    has_feature_of_interest = OID(res_ids[key]),
                )
        observations.append(o)
    

    instance = tp.air_quality_measurement(
        label = name,
        occurs_in = tp.measurement_location(
            #label = common_name,
            label = "Location",
            city = OID(city_oid),
            country = OID(country_oid),
            geographical_region = OID(region_oid),
            additional_information = additional,
        ),
        stringencyindex = sIndex,
        measurement_platform = platform,
        method = method,
        has_observations = observations,
        has_time = tp.time_interval(
            has_beginning = datetime.strptime(start,'%Y-%m-%d').date(),
            has_end = datetime.strptime(end,'%Y-%m-%d').date(),
            ), 
    )
    instance.serialize_to_file(path+"contribution"+str(con_number)+".json", format="json-ld")

def create_JSON_data():
    '''
    This function creates all necessary json files for each paper and contribution.
    Each DOI has a directory with the metadata and a directory for the contributions in it
    '''
    currentDOI = ""
    con_number = 1
    
    for index, row in df.iterrows(): 
        #make a directory for each new paper
        if (not os.path.isdir(str(row["DOI"]).replace("/",""))):
            currentDOI = str(row["DOI"])
            os.mkdir(currentDOI.replace("/",""))
            con_number = 1
            #make JSON-File for the metadata
            get_crossref_data(currentDOI.replace("/",""),currentDOI)
        #now make JSON-File for each contribution
        print(currentDOI)
        if not os.path.isdir(currentDOI.replace("/","")+"/contributions"):
            os.mkdir(currentDOI.replace("/","")+"/contributions")
        create_contribution(currentDOI.replace("/","") + "/contributions/",row,column_names,con_number)
        con_number += 1

def upload_papers():
    '''
    This function creates new papers in ORKG. 
    Since the available metadata differs, the non-required data is added via the statement client.
    The paper ids are saved in a json-file, to be used for other purposes.
    '''
    print("start uploading:")
    for index, row in df2.iterrows():
        if str(row["DOI"]) in paper_ids:
            continue
        _doi = None
        _title = None
        _published_in = None
        _author = None
        _publication_year = None
        _publication_month = None
        path = str(row["DOI"]).replace("/","")
        print(path)
        if os.path.isfile(path + '/metadata.json'):
            with open(path + '/metadata.json') as f:
                metadata_dict = json.load(f)
                _title = metadata_dict["message"]["title"][0]
                response = orkg.harvesters.directory_harvest(
                            directory=path+'/contributions',
                            research_field=OID('R145'),
                            title = _title,
                )
                if "DOI" in metadata_dict["message"]:
                    _doi = metadata_dict["message"]["DOI"]
                    _ID = orkg.literals.add(label=_doi).content["id"]
                    orkg.statements.add(subject_id=response.content["id"],predicate_id="P26",object_id = _ID)
                if len(metadata_dict["message"]["container-title"]) > 0:
                    _published_in = metadata_dict["message"]["container-title"][0]
                    _ID = orkg.literals.add(label=_published_in).content["id"]
                    orkg.statements.add(subject_id=response.content["id"],predicate_id="HAS_VENUE",object_id = _ID)
                if "created" in metadata_dict["message"]:
                    _publication_year = metadata_dict["message"]["created"]["date-parts"][0][0]
                    _publication_month = metadata_dict["message"]["created"]["date-parts"][0][1]
                    _ID = orkg.literals.add(label=_publication_year).content["id"]
                    orkg.statements.add(subject_id=response.content["id"],predicate_id="P29",object_id = _ID)
                    _ID = orkg.literals.add(label=_publication_month).content["id"]
                    orkg.statements.add(subject_id=response.content["id"],predicate_id="P28",object_id = _ID)
                if "author" in metadata_dict["message"]:
                    if "given" in metadata_dict["message"]["author"][0]:
                        _author = metadata_dict["message"]["author"][0]["given"] +" "+ metadata_dict["message"]["author"][0]["family"]
                    elif "family" in metadata_dict["message"]["author"][0]:
                        _author = metadata_dict["message"]["author"][0]["family"]
                    _ID = orkg.literals.add(label=_author).content["id"]
                    list_ID = orkg.lists.add(label="authors list", elements=[_ID]).content["id"]
                    orkg.statements.add(subject_id=response.content["id"],predicate_id="hasAuthors",object_id =list_ID)
        else:
            _title = metadata_df.loc[metadata_df['DOI'] == path]['Title'].values[0]
            _author = metadata_df.loc[metadata_df['DOI'] == path]['Author'].values[0]
            _publication_month = metadata_df.loc[metadata_df['DOI'] == path]['PublicationMonth'].values[0]
            _publication_year = metadata_df.loc[metadata_df['DOI'] == path]['PublicationYear'].values[0]
            _published_in = metadata_df.loc[metadata_df['DOI'] == path]['PublishedIn'].values[0]
            response = orkg.harvesters.directory_harvest(
                directory= path + '/contributions',
                research_field= OID('R145'),
                title= _title
            )
            if not  _author == 'nan':
                _ID = orkg.literals.add(label=_author).content["id"]
                list_ID = orkg.lists.add(label="authors list", elements=[_ID]).content["id"]
                orkg.statements.add(subject_id=response.content["id"],predicate_id="hasAuthors",object_id =list_ID)
            if not _publication_month == 'nan':
                _ID = orkg.literals.add(label=_publication_month).content["id"]
                orkg.statements.add(subject_id=response.content["id"],predicate_id="P28",object_id = _ID)
            if not _publication_year == 'nan':
                _ID = orkg.literals.add(label=_publication_year).content["id"]
                orkg.statements.add(subject_id=response.content["id"],predicate_id="P29",object_id = _ID)
            if not _published_in == 'nan':
                _ID = orkg.literals.add(label=_published_in).content["id"]
                orkg.statements.add(subject_id=response.content["id"],predicate_id="HAS_VENUE",object_id = _ID)
        #print(response.content)
        if 'id' in response.content:
            paper_ids[str(row["DOI"])] = response.content["id"]
            with open("papers_ids.json","w") as f:
                json.dump(paper_ids,f)
    
def delete_papers():
    '''
    shortcut to remove all papers from orkg
    '''
    with open("papers_ids.json","r") as f:
        id_dict = json.load(f)
    id_list = list(id_dict.values())
    for index, row in df.iterrows():
        if os.path.isdir(str(row["DOI"]).replace("/","")):
            shutil.rmtree(str(row["DOI"]).replace("/",""))
    
    for id in id_list:
        if orkg.resources.exists(id):
            id_list = []
            for s in orkg.statements.get_by_subject(id).content:
                id_list.append(s["id"])
        response = requests.delete("https://incubating.orkg.org/api/statements/?ids="+ str(id_list).replace("[",""))
    os.remove("papers_ids.json")

def get_contributions(key,value,pollutant):
    '''
    gets all the DOIs of papers by a certain key in the Dataframe
    :param key: key in the input Dataframe
    :param value: the value which the column entry should match
    :return: list of all DOIs matching the value in the key-column
    '''
    doi_dict = {}
    result = {}
    values = list(df[key])
    tmp_df = df.loc[df[key] == value]
    #print(tmp_df)
    for v in values:
        doi_dict[v] = list(dict.fromkeys(list(df.loc[df[key] == v]["DOI"].values)))
    for k in doi_dict:
        result[k] = []
        for e in doi_dict[k]:
            for r in orkg.papers.by_doi(e).content:
                if "641d08b6-ef9b-4bd6-b900-bfe4763bb23c" in r["created_by"]:
                    for c in r["contributions"]:
                        result[k].append(c["id"])
    return filter_contributions(result[value],key,value,pollutant)

def filter_contributions(id_list,key,value,pollutant):
    #filter the contributions by the the given value 
    result = []
    for element in id_list:
        response = orkg.statements.get_by_subject_and_predicate(subject_id=element,predicate_id="OCCURS_IN").content
        tmp = []
        for r in response:
            tmp.append(r["object"]["id"])
        for o in tmp:
            for res in orkg.statements.get_by_subject_and_predicate(subject_id=o,predicate_id=key_OID_dict[key]).content:
                if value in res["object"]["label"]:
                    for r in orkg.statements.get_by_subject_and_predicate(subject_id=element,predicate_id="P7085").content:
                        if pollutant in r["object"]["label"]:
                            result.append(element)
    return list(dict.fromkeys(result))
          
def publish_comparison(contribution_list,value_list):
    loc = ""
    for i  in range(len(value_list)):
        if i != 0:
            loc += "/" + value_list[i]
        else:
            loc += value_list[i]
    title = "Air Pollution Measurement during Covid-19 lockdown for " + loc
    description = "Compares the air quality during lockdown for PM2.5 in " + loc
    response = orkg.comparisons.publish(
        contribution_ids=contribution_list,
        title=title,
        description=description
    )
    if "id" in response.content:
        com_ids[response.content["id"]] = str(title)
        with open("comparison_ids.json","w") as f:
            json.dump(com_ids,f)
    else:
        print(response.content)

def organize_data():
    files = os.listdir('comparison_data')
    files.remove('.DS_Store')
    asia = {}
    europe = {}
    america = {}
    africa_oceania = {}
    for f in files:
        com_df = pd.read_csv("comparison_data/" +f).astype(str)
        if "Asia" in f:
            for index, row in com_df.iterrows():
                if row.loc["occurs in/location/country"] in asia.keys():
                    if "None" in row.loc["has observations/pm25/has result"] or "result" in row.loc["has observations/pm25/has result"]:
                        continue
                    try:
                        asia[row.loc["occurs in/location/country"]].append(float(row["has observations/pm25/has result"].split(",")[2].split(":")[1].replace("%","")))
                    except:
                        asia[row.loc["occurs in/location/country"]].append(float(row["has observations/pm25/has result"].split(" ")[0]))
                else:
                    if "None" in row.loc["has observations/pm25/has result"] or "result" in row.loc["has observations/pm25/has result"]:
                        continue
                    try:
                        asia[row.loc["occurs in/location/country"]] = [float(row["has observations/pm25/has result"].split(",")[2].split(":")[1].replace("%",""))]
                    except:
                        asia[row.loc["occurs in/location/country"]] = [float(row["has observations/pm25/has result"].split(" ")[0])]
        elif "Europe" in f:
            for index, row in com_df.iterrows():
                if row.loc["occurs in/location/country"] in europe.keys():
                    if "None" in row.loc["has observations/pm25/has result"] or "result" in row.loc["has observations/pm25/has result"]:
                        continue
                    try:
                        europe[row.loc["occurs in/location/country"]].append(float(row["has observations/pm25/has result"].split(",")[2].split(":")[1].replace("%","")))
                    except:
                        europe[row.loc["occurs in/location/country"]].append(float(row["has observations/pm25/has result"].split(" ")[0]))
                else:
                    if "None" in row.loc["has observations/pm25/has result"] or "result" in row.loc["has observations/pm25/has result"]:
                        continue
                    try:
                        europe[row.loc["occurs in/location/country"]] = [float(row["has observations/pm25/has result"].split(",")[2].split(":")[1].replace("%",""))]
                    except:
                        europe[row.loc["occurs in/location/country"]] = [float(row["has observations/pm25/has result"].split(" ")[0])]
        elif "America" in f:
            for index, row in com_df.iterrows():
                if row.loc["occurs in/location/country"] in america.keys():
                    if "None" in row.loc["has observations/pm25/has result"] or "result" in row.loc["has observations/pm25/has result"]:
                        continue
                    try:
                        america[row.loc["occurs in/location/country"]].append(float(row["has observations/pm25/has result"].split(",")[2].split(":")[1].replace("%","")))
                    except:
                        america[row.loc["occurs in/location/country"]].append(float(row["has observations/pm25/has result"].split(" ")[0]))
                else:
                    if "None" in row.loc["has observations/pm25/has result"] or "result" in row.loc["has observations/pm25/has result"]:
                        continue
                    try:
                        america[row.loc["occurs in/location/country"]] = [float(row["has observations/pm25/has result"].split(",")[2].split(":")[1].replace("%",""))]
                    except:
                        america[row.loc["occurs in/location/country"]] = [float(row["has observations/pm25/has result"].split(" ")[0])]
        elif "Africa" in f or "Oceania" in f:
            for index, row in com_df.iterrows():
                if row.loc["occurs in/location/country"] in africa_oceania.keys():
                    if "None" in row.loc["has observations/pm25/has result"] or "result" in row.loc["has observations/pm25/has result"]:
                        continue
                    try:
                        africa_oceania[row.loc["occurs in/location/country"]].append(float(row["has observations/pm25/has result"].split(",")[2].split(":")[1].replace("%","")))
                    except:
                        africa_oceania[row.loc["occurs in/location/country"]].append(float(row["has observations/pm25/has result"].split(" ")[0]))
                else:
                    if "None" in row.loc["has observations/pm25/has result"] or "result" in row.loc["has observations/pm25/has result"]:
                        continue
                    try:
                        africa_oceania[row.loc["occurs in/location/country"]] = [float(row["has observations/pm25/has result"].split(",")[2].split(":")[1].replace("%",""))]
                    except:
                        africa_oceania[row.loc["occurs in/location/country"]] = [float(row["has observations/pm25/has result"].split(" ")[0])]
    for k in europe:
        europe[k] = statistics.median(europe[k])
    for k in asia:
        asia[k] = statistics.median(asia[k])
    for k in america:
        america[k] = statistics.median(america[k])
    for k in africa_oceania:
        africa_oceania[k] = statistics.median(africa_oceania[k])
    with open("Europe.json","w") as f:
        json.dump(europe,f)
    with open("Asia.json","w") as f:
        json.dump(asia,f)
    with open("America.json","w") as f:
        json.dump(america,f)
    with open("Africa_Oceania.json","w") as f:
        json.dump(africa_oceania,f)
    return ["Europe.json","Africa_Oceania.json","America.json","Asia.json"]

def get_stringency_index():
    s_index = {}
    for index, row in df.iterrows():
        if row.loc["Country"] in s_index:
            s_index[row.loc["Country"]].append(row.loc["StringencyIndex"])
        else:
            s_index[row.loc["Country"]] = [row.loc["StringencyIndex"]]
    for k in s_index:
        s_index[k] = statistics.median(s_index[k])
    print(s_index)
    with open("Stringency_Index.json","w") as f:
        json.dump(s_index,f)

def visualize_comparison():
    with open("Stringency_Index.json") as f:
        d = json.load(f)
        index_df = pd.DataFrame(d.items(),columns = ["Country","StringencyIndex"])
    with open("Asia.json","r") as f:
        asia = json.load(f)
        del asia["nan"]
    with open("America.json","r") as f:
        america = json.load(f)
    with open("Africa_Oceania.json","r") as f:
        africa_oceania = json.load(f)
        del africa_oceania["nan"]
    with open("Europe.json","r") as f:
        europe = json.load(f)
        del europe["nan"]
    fig = plt.figure(1,layout="constrained")
    fig.suptitle("PM2.5")
    colors_asia = []
    colors_america = []
    colors_europe = []
    colors_af_oc = []
    #get 
    for k in asia:
        colors_asia.append(index_df.loc[index_df["Country"] == k, "StringencyIndex"].item())
    for k in america:
        colors_america.append(index_df.loc[index_df["Country"] == k, "StringencyIndex"].item())
    for k in europe:
        colors_europe.append(index_df.loc[index_df["Country"] == k, "StringencyIndex"].item())
    for k in africa_oceania:
        colors_af_oc.append(index_df.loc[index_df["Country"] == k, "StringencyIndex"].item())
    gridspec.GridSpec(4,3)
    #Europe
    plt.subplot2grid((4,3),(0,0),rowspan=4,colspan=1)
    plt.title("Europe")
    plt.scatter(list(europe.values()),list(europe.keys()),c=colors_europe,cmap=plt.cm.get_cmap("jet",10))
    cbar = plt.colorbar(orientation="vertical")
    cbar.set_label("Stringency Index")
    plt.clim(0, 100)
    plt.axis(xmin=-90,xmax=40)
    #Africa/Oceania
    plt.subplot2grid((4,3),(0,1),rowspan=1,colspan=1)
    plt.title("Africa and Oceania")
    plt.scatter(list(africa_oceania.values()),list(africa_oceania.keys()),c=colors_af_oc,cmap=plt.cm.get_cmap("jet",10))
    plt.axis(xmin=-90,xmax=40)
    #Asia
    plt.subplot2grid((4,3),(1,1),rowspan=3,colspan=1)
    plt.title("Asia")
    plt.scatter(list(asia.values()),list(asia.keys()),c=colors_asia,cmap=plt.cm.get_cmap("jet",10))
    plt.axis(xmin=-90,xmax=40)
    #America
    plt.subplot2grid((4,3),(0,2),rowspan=2,colspan=1)
    plt.title("America")
    plt.scatter(list(america.values()),list(america.keys()),c=colors_america,cmap=plt.cm.get_cmap("jet",10))
    plt.axis(xmin=-90,xmax=40)
    
    #fig.tight_layout()
    fig.savefig("PM25figure.png")
    plt.show()

def recreate_original_csv():
    '''
    fetch all the contribution data by creating smaller comparisons.
    Comparison size is set to 20 contributions each.
    '''
    comparisons_dict = {}
    contribution_list = []
    #get all contribution ids 
    for id in paper_ids.keys():
        for r in orkg.papers.by_doi(id).content:
            for con in r["contributions"]:
                #filter out papers uploaded by Quentin
                if not ("Observation" in con["label"] or "Obersavtion" in con["label"]):
                    contribution_list.append(con["id"])
    print(len(contribution_list))

    comparison_number = 1
    for i in range(0,len(contribution_list),20):
        title = "Air Quality Measurement during Covid-19 Lockdown Synthesis Number " + str(comparison_number)
        if i+20 > len(contribution_list):
            response = orkg.comparisons.publish(contribution_ids=contribution_list[i:len(contribution_list)],title=title)
            print(response.content)
        else:
            response = orkg.comparisons.publish(contribution_ids=contribution_list[i:i+20],title=title) 
            print(response.content)    
        comparisons_dict[str(comparison_number)] = response.content["id"]
        comparison_number += 1

    with open("comparisons_test.json","w") as f:
        json.dump(comparisons_dict,f)

def assemble_csv():
    '''
    organize the data created by the function recreate_original_csv 
    in a way that matches the original input csv-file
    :return a pandas Dataframe 
    '''
    df_list = []
    for file in os.listdir("Comparison_test_data"):
        if not ".DS" in file:
            tmp = pd.read_csv("Comparison_test_data/" + file).astype(str)
            df_list.append(tmp)
    csv_df = pd.concat(df_list)
    print(csv_df.info())
    print(csv_df.shape)
    result = pd.DataFrame(columns=["StudyStartDate","StudyEndDate","FirstAuthor","DOI","AdditionalDetails","City","Country","GeographicalRegion","methods","platform","StringencyIndex","NO2_prcnt_change","NO2_prcnt_change_er","NO2_ugm3_Reference_avg","NO2_ugm3_Reference_sd","NO2_ugm3_Lockdown_avg","NO2_ugm3_Lockdown_sd","NOX_prcnt_change","NOX_prcnt_change_er","NOX_ugm3_Reference_avg","NOX_ugm3_Reference_sd","NOX_ugm3_Lockdown_avg","NOX_ugm3_Lockdown_sd","CO_prcnt_change","CO_prcnt_change_er","CO_mgm3_Reference_avg","CO_mgm3_Reference_sd","CO_mgm3_Lockdown_avg","CO_mgm3_Lockdown_sd","PM25_prcnt_change","PM25_prcnt_change_er","PM25_ugm3_Reference_avg","PM25_ugm3_Reference_sd","PM25_ugm3_Lockdown_avg","PM25_ugm3_Lockdown_sd","PM10_prcnt_change","PM10_prcnt_change_er","PM10_ugm3_Reference_avg","PM10_ugm3_Reference_sd","PM10_ugm3_Lockdown_avg","PM10_ugm3_Lockdown_sd","O3_prcnt_change","O3_prcnt_change_er","O3_ugm3_Reference_avg","O3_ugm3_Reference_sd","O3_ugm3_Lockdown_avg","O3_ugm3_Lockdown_sd","SO2_prcnt_change","SO2_prcnt_change_er","SO2_ugm3_Reference_avg","SO2_ugm3_Reference_sd","SO2_ugm3_Lockdown_avg","SO2_ugm3_Lockdown_sd","NH3_prcnt_change","NH3_prcnt_change_er","NH3_ugm3_Reference_avg","NH3_ugm3_Reference_sd","NH3_ugm3_Lockdown_avg","NH3_ugm3_Lockdown_sd","NMVOCS_prcnt_change","NMVOCS_prcnt_change_er","NMVOCS_ugm3_Reference_avg","NMVOCS_ugm3_Reference_sd","NMVOCS_ugm3_Lockdown_avg","NMVOCS_ugm3_Lockdown_sd","AOD_prcnt_change","AOD_prcnt_change_er","AOD_Reference_avg","AOD_Reference_sd","AOD_Lockdown_avg","AOD_Lockdown_sd","BC_prcnt_change","BC_prcnt_change_er","BC_ugm3_Reference_avg","BC_ugm3_Reference_sd","BC_ugm3_Lockdown_avg","BC_ugm3_Lockdown_sd","AQI_prcnt_change","AQI_prcnt_change_er","AQI_Reference_avg","AQI_Reference_sd","AQI_Lockdown_avg","AQI_Lockdown_sd"],dtype=str)
    columns = csv_df.columns
    for index,row in csv_df.iterrows():
        for i, element in enumerate(row):
            if str(element) == "nan":
                continue
            column_name = columns[index]
            if "has time" in column_name:
                if "has beginning" in column_name:
                    result.loc[index,"StudyStartDate"] = element
                elif "has end" in column_name:
                    result.loc[index,"StudyEndDate"] = element
            elif "additional information" in column_name:
                result.loc[index,"AdditionalDetails"] = element
            elif "city" in column_name:
                result.loc[index,"City"] = element
            elif "country" in column_name:
                result.loc[index,"country"] = element
            elif "region" in column_name:
                result.loc[index,"GeographicalRegion"] = element
            elif "method" in column_name:
                result.loc[index,"methods"] = element
            elif "platform" in column_name:
                result.loc[index,"platform"] = element
            elif "stringencyindex" in column_name:
                result.loc[index,"StringencyIndex"] = element
            elif "has observations/no2/has result" in column_name:
                try:
                    result.loc[index,"NO2_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"NO2_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1].replace("ugm3","")
                    result.loc[index,"NO2_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1].replace("ugm3","")
                except:
                    result.loc[index,"NO2_prcnt_change"] = element.split(" ")[0]
            elif "has observations/nox/has result" in column_name:
                try:
                    result.loc[index,"NOX_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"NOX_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1].replace("ugm3","")
                    result.loc[index,"NOX_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1].replace("ugm3","")
                except:
                    result.loc[index,"NOX_prcnt_change"] = element.split(" ")[0]
            elif "has observations/co/has result" in column_name:
                try:
                    result.loc[index,"CO_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"CO_mgm3_Reference_avg"] = element.split(",")[0].split(":")[1].replace("mgm3","")
                    result.loc[index,"CO_mgm3_Lockdown_avg"] = element.split(",")[1].split(":")[1].replace("mgm3","")
                except:
                    result.loc[index,"CO_prcnt_change"] = element.split(" ")[0]
            elif "has observations/pm25/has result" in column_name:
                try:
                    result.loc[index,"PM25_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"PM25_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1].replace("ugm3","")
                    result.loc[index,"PM25_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1].replace("ugm3","")
                except:
                    result.loc[index,"PM25_prcnt_change"] = element.split(" ")[0]
            elif "has observations/pm10/has result" in column_name:
                try:
                    result.loc[index,"PM10_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"PM10_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1].replace("ugm3","")
                    result.loc[index,"PM10_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1].replace("ugm3","")
                except:
                    result.loc[index,"PM10_prcnt_change"] = element.split(" ")[0]
            elif "has observations/o3/has result" in column_name:
                try:
                    result.loc[index,"O3_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"O3_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1].replace("ugm3","")
                    result.loc[index,"O3_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1].replace("ugm3","")
                except:
                    result.loc[index,"O3_prcnt_change"] = element.split(" ")[0]
            elif "has observations/so2/has result" in column_name:
                try:
                    result.loc[index,"SO2_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"SO2_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1].replace("ugm3","")
                    result.loc[index,"SO2_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1].replace("ugm3","")
                except:
                    result.loc[index,"SO2_prcnt_change"] = element.split(" ")[0]
            elif "has observations/nh3/has result" in column_name:
                try:
                    result.loc[index,"NH3_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"NH3_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1].replace("ugm3","")
                    result.loc[index,"NH3_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1].replace("ugm3","")
                except:
                    result.loc[index,"NH3_prcnt_change"] = element.split(" ")[0]
            elif "has observations/nmvocs/has result" in column_name:
                try:
                    result.loc[index,"NMVOCS_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"NMVOCS_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1].replace("ugm3","")
                    result.loc[index,"NMVOCS_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1].replace("ugm3","")
                except:
                    result.loc[index,"NMVOCS_prcnt_change"] = element.split(" ")[0]
            elif "has observations/aod/has result" in column_name:
                try:
                    result.loc[index,"AOD_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"AOD_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1]
                    result.loc[index,"AOD_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1]
                except:
                    result.loc[index,"AOD_prcnt_change"] = element.split(" ")[0]
            elif "has observations/bc/has result" in column_name:
                try:
                    result.loc[index,"BC_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"BC_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1].replace("ugm3","")
                    result.loc[index,"BC_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1].replace("ugm3","")
                except:
                    result.loc[index,"BC_prcnt_change"] = element.split(" ")[0]
            elif "has observations/aqi/has result" in column_name:
                try:
                    result.loc[index,"AQI_prcnt_change"] = element.split(",")[2].split(":")[1].replace("%","")
                    result.loc[index,"AQI_ugm3_Reference_avg"] = element.split(",")[0].split(":")[1]
                    result.loc[index,"AQI_ugm3_Lockdown_avg"] = element.split(",")[1].split(":")[1]
                except:
                    result.loc[index,"AQI_prcnt_change"] = element.split(" ")[0]
    print(result.info())
    print(result.shape)

'''
un-comment if necessary
'''
#delete_papers()
#create_JSON_data()
#upload_papers()
#get_stringency_index()
#organize_data()
#visualize_comparison()
#recreate_original_csv()
#assemble_csv()