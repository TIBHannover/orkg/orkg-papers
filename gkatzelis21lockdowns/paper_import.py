import pandas as pd
from orkg import ORKG
import requests
import urllib.parse
import time
import json
import re

from qwikidata.sparql import return_sparql_query_results

# specify the filename for input data
inputFile = "covid-aqs-database_2023-01-26_version_5.csv"
property_ids = {}
resource_ids = {}
location_wikidata_ids = {}
metadata = ['authors', 'publishedIn', 'title', 'abstract', 'publicationYear', 'publicationMonth', 'researchField', 'contributions']


def get_crossref_data(doi: str, index: int):
    """
    This function calls the crossref API to retrieve metadata for a given doi.
    :param doi: the doi that should be queried
    :param index: index of current paper in dataframe
    :return: dictionary containing api data
    """

    if not doi:
        return {}

    crossref_url = 'https://api.crossref.org/works/' + str(doi)

    try:
        # print(doi)
        response = requests.get(crossref_url)

    except Exception as e:
        print(e)
        print("got connection error in crossref, trying again...")
        time.sleep(60)
        try:
            response = requests.get(crossref_url)
        except Exception as e:
            print(e)
            print("got connection error in crossref, leaving out")

    data_dict = {}
    if response.ok:
        content_dict_crossref = json.loads(response.content)
        message = content_dict_crossref['message']
        # print(message)

        data_dict = _process_api_data_crossref(message, index)

    # print(data_dict)
    return data_dict


def _process_api_data_crossref(message, index):
    """
    This function processes the api data retrieved from the api query.
    :param message: the message returned by the api call
    :param index: index of current paper in dataframe
    :return: dictionary containing api data
    """

    data_dict = {}
    abstract = process_abstract_string(message.get('abstract', ''))

    data_dict['abstract'] = abstract
    data_dict["authors"] = message.get("author", "")
    data_dict['publishedIn'] = message.get('container-title', '')
    if len(data_dict["publishedIn"]) == 0:
        data_dict["publishedIn"] = message["publisher"]
    data_dict['title'] = message.get('title', '')
    try:
        data_dict["publicationYear"] = message["published-print"]["date-parts"][0][0]  # year
        data_dict["publicationMonth"] = message["published-print"]["date-parts"][0][1]  # month
    except:
        try:
            data_dict["publicationYear"] = message["published-online"]["date-parts"][0][0]  # year
            data_dict["publicationMonth"] = message["published-online"]["date-parts"][0][1]  # month
        except:
            try:
                data_dict["publicationYear"] = message["created"]["date-parts"][0][0]  # year
                data_dict["publicationMonth"] = message["created"]["date-parts"][0][1]  # month
            except:
                print("date")
                print(message)

    return data_dict


def process_abstract_string(abstract: str) -> str:
    """
    This function processes the abstract by removing unwanted artefacts
    :param abstract: the abstract that should be processed
    :return: cleaned abstract
    """

    if not abstract:
        return ''

    # replace
    abstract = abstract.replace('\n', '')
    abstract = abstract.replace('\t', '')
    abstract = abstract.replace('\r', '')

    abstract_pattern = re.compile("abstract", re.IGNORECASE)
    jats_pattern = '</?jats:[a-zA-Z0-9_]*>'
    replace = ''

    abstract = re.sub(abstract_pattern, replace, abstract)
    abstract = re.sub(jats_pattern, replace, abstract).strip()

    return abstract


def fetch_meta_data(df):
    """
    This function retrieves metadata for each paper by calling the crossref api.
    This is necessary to be able to add a paper to the orkg.
    :param df: the original dataframe containing the papers
    :return: updated dataframe now containing metadata information
    """
    doi_dict = {}
    df["authors"] = ""
    df["publishedIn"] = ""
    df["title"] = ""
    df["abstract"] = ""
    df["publicationYear"] = ""
    df["publicationMonth"] = ""
    not_found_df = pd.DataFrame(columns=df.columns)

    for i, r in df.iterrows():

        # if we already gathered metadata for the paper, we do not need to query the API a second time
        if r["doi"] in doi_dict.keys():
            crossref_data = doi_dict[r["doi"]]
        else:
            crossref_data = get_crossref_data(r["doi"], i)

            if crossref_data == {}:
                not_found_df = pd.concat([not_found_df, r])
                print(f"nothing found for {r['doi']}")
                continue
            else:
                doi_dict[r["doi"]] = crossref_data

        # print(crossref_data)

        # add the retrieved data to our dataframe
        df.at[i, "authors"] = crossref_data["authors"]
        df.at[i, "publishedIn"] = crossref_data["publishedIn"][0]
        df.at[i, "title"] = crossref_data["title"][0]
        df.at[i, "abstract"] = crossref_data["abstract"]
        df.at[i, "publicationYear"] = crossref_data["publicationYear"]
        df.at[i, "publicationMonth"] = crossref_data["publicationMonth"]

        # convert the author's name formatting
        authors = []
        for author in crossref_data["authors"]:
            author_family = author["family"]
            if "given" in author:
                author_given = author["given"]
                if "middle" in author:
                    author_given += " " + author["middle"]
                author_family = author_given + " " + author_family
            current_author = {"label": author_family}
            authors.append(current_author)
        df.at[i, "authors"] = authors

    not_found_df.to_csv("no_meta_data.csv", index=False)
    return df


def get_column_names(df):
    """
    This function returns the names of columns of a dataframe.
    :param df: the dataframe that is being observed
    :return: list of column names
    """
    return list(df.columns)


def add_meta_data_paper(paper, row, column_names):
    """
    This function add metadata to a given paper.
    :param paper: the paper construct that needs metadata
    :param row: the row containing the metadata
    :param column_names: column names
    :return: paper containing metadata
    """
    column_index = 0
    for element in row:
        if not str(element) == "nan":
            column_name = column_names[column_index]
            if column_name in metadata:
                paper["paper"][column_name] = element

        column_index += 1

    return paper


def add_predicates_to_orkg(orkg):
    """
    This function adds all necessary predicates to the orkg and saves the mapping to a file.
    :param orkg: orkg client
    :return:
    """
    predicate_ids = {}

    # all of these predicates should already be present in the orkg
    predicate_ids["has time"] = "P43102"
    predicate_ids["has component"] = "P37641"
    predicate_ids["has unit"] = "P7004"
    predicate_ids["numeric value"] = "wikidata:P1181"
    predicate_ids["occurs in"] = "OCCURS_IN"
    predicate_ids["has beginning"] = "P15699"
    predicate_ids["has end"] = "P15700"
    predicate_ids["has specified input"] = "P4015"
    predicate_ids["has specified output"] = "P4003"
    predicate_ids["methods"] = "P1005"

    # create predicates for relative change, specified input/output, methods, measurement platform, StringencyIndex (including description?)
    creation_list = ["has relative change", "measurement platform", "StringencyIndex"]
    for element in creation_list:
        response = orkg.predicates.add(label=element)
        predicate_ids[element] = response.content["id"]

    # save ids to a file for later reference
    with open("predicate_ids.json", "w") as f:
        json.dump(predicate_ids, f)


def get_predicate_ids():
    """
    This function initialises the property_ids dictionary by reading in the previously created predicate ids file.
    :return:
    """
    with open("predicate_ids.json", "r") as f:
        predicate_ids = json.load(f)
    
    for key, value in predicate_ids.items():
        property_ids[key] = value


def add_resources_to_orkg(orkg):
    """
    This function adds all necessary resources to the orkg and saves the mapping to a file.
    :param orkg: orkg client
    :return:
    """
    df = pd.read_csv(inputFile, sep=";")

    # we need to add the different methods and measurement platforms as resources
    df = df[["methods", "platform"]]

    resources = []
    for column in df.columns:
        resources.extend(df[column].unique())

    # remove the platform "both" -> we will add both platforms if the keyword was "both"
    resources.pop(-1)

    # add the resources to the orkg
    res_ids = {}
    for name in resources:
        result = orkg.resources.add(label=name)
        res_ids[name] = result.content["id"]

    # add unit ids
    res_ids["prcnt"] = "wikidata:Q11229"
    res_ids["ugm3"] = "wikidata:Q106623548"
    res_ids["mgm3"] = "wikidata:Q21077820"

    # add city, big city and country ids
    res_ids["city"] = "wikidata:Q515"
    res_ids["big city"] = "wikidata:Q1549591"
    res_ids["country"] = "wikidata:Q6256"

    # TODO: make sure they are not already existing in the orkg or you'll get duplicate links
    # create resources not yet existing in orkg (percent should already exist, and we don't need the location resources)
    ugm3_id = res_ids["ugm3"]
    mgm3_id = res_ids["mgm3"]

    orkg.resources.add(id=ugm3_id, label="micrograms per cubic metre")
    literal = orkg.literals.add(label=f"https://www.wikidata.org/entity/{ugm3_id.split(':')[1]}")
    orkg.statements.add(subject_id=ugm3_id, predicate_id="SAME_AS", object_id=literal.content["id"])

    orkg.resources.add(id=mgm3_id, label="milligrams per cubic metre")
    literal = orkg.literals.add(label=f"https://www.wikidata.org/entity/{mgm3_id.split(':')[1]}")
    orkg.statements.add(subject_id=mgm3_id, predicate_id="SAME_AS", object_id=literal.content["id"])

    # save ids to a file for later reference
    with open("resource_ids.json", "w") as f:
        json.dump(res_ids, f)


def get_resource_ids():
    """
    This function initialises the resource_ids dictionary by reading in the previously created resource ids file.
    :return:
    """
    with open("resource_ids.json", "r") as f:
        res_ids = json.load(f)

    for key, value in res_ids.items():
        resource_ids[key] = value


def get_contribution_ids(orkg, paper_id):
    """
    This function creates a list of all the contributions belonging to a given paper.
    :param orkg: orkg client
    :param paper_id: id of the paper to be analysed
    :return: list of all contribution ids for the given paper
    """
    contribution_ids = []
    predicate_id = "P31"  # predicate "has contribution"
    statements = orkg.statements.get_by_subject(subject_id=paper_id, size=500)
    for statement in statements.content:
        if statement["predicate"]["id"] == predicate_id:
            contribution_ids.append(statement["object"]["id"])
    return contribution_ids


def new_paper_upload_function(orkg):
    """
    This function is the main function for the upload of the data.
    First it fetches metadata, then it adds each paper to the orkg containing the pollutant data as contributions.
    :param orkg: orkg client
    :return:
    """

    df = pd.read_csv(inputFile, sep=";")

    # rename DOI to doi, remove FirstAuthor column, research field
    df = df.rename(columns={"DOI": "doi"})
    df = df.drop("FirstAuthor", axis=1)
    df["researchField"] = "R145"

    # add meta data to data frame
    df = fetch_meta_data(df)

    df.to_csv("metadata.csv", index=False)
    print("fetched meta data")

    # get the column names so we can easily reference them
    column_names = get_column_names(df)

    # start by initialising an empty paper
    last_doi = ""
    started = False
    paper = {"paper": {}}
    same_paper_index = 0
    same_paper_df = pd.DataFrame()
    paper_id = ""
    contributions = []
    for index, row in df.iterrows():
        # skip if no meta data
        if len(row["authors"]) == 0:
            continue

        same_paper_index += 1
        # if finished, i.e. it's a new paper then reset parameters
        if index > 0 and last_doi != row["doi"]:

            # add accumulated contributions to the paper and upload it
            paper["paper"]["contributions"] = contributions
            print(paper)
            added_paper = orkg.papers.add(params=paper)
            paper_id = added_paper.content["id"]

            # get contributions for the paper and add the actual values
            contribution_ids = get_contribution_ids(orkg, paper_id)
            # print(contribution_ids)
            # print(len(same_paper_df.index))

            for same_index, same_row in same_paper_df.iterrows():
                add_contribution_to_paper(contribution_ids[same_index], same_row, column_names, orkg)

            # reset parameters
            paper = {"paper": {}}
            same_paper_index = 1
            started = False
            contributions = []

        # if it is the first contribution for the paper then start with adding the metadata
        if not started:
            print("#####")
            print(f"new paper {paper_id}")

            paper = add_meta_data_paper(paper, row, column_names)

            # create a sub dataframe containing only rows with the same doi as the current
            same_paper_df = df.loc[df["doi"] == row["doi"]].reset_index(drop=True)
            started = True

        # create new contribution for each row of the same paper
        contributions.append({"name": f"Observation {same_paper_index}"})

        # add_contribution_to_paper(paper_id, row, column_names, orkg, contribution_label)
        last_doi = row["doi"]

    # add the last paper and its contributions because we exit the loop without adding the paper
    paper["paper"]["contributions"] = contributions
    added_paper = orkg.papers.add(params=paper)
    paper_id = added_paper.content["id"]

    # get contributions for the paper and add the actual values
    contribution_ids = get_contribution_ids(orkg, paper_id)
    for same_index, same_row in same_paper_df.iterrows():
        add_contribution_to_paper(contribution_ids[same_index], same_row, column_names, orkg)


def add_contribution_to_paper(contribution_id, row, column_names, orkg):
    """
    This function adds information to a given contribution.
    It parses a given row and adds each important value to the contribution.
    :param contribution_id: the id of the contribution that we are editing
    :param row: the row containing the information to be added
    :param column_names: column names
    :param orkg: orkg client
    :return:
    """
    relative_values = False
    reference_values = False
    lockdown_values = False
    relative_id = ""
    reference_id = ""
    lockdown_id = ""

    has_beginning = None
    has_end = None
    city = ""
    country = ""

    for column_index, element in enumerate(row):
        if str(element) == "nan":
            continue

        column_name = column_names[column_index]

        if column_name == "StudyStartDate":
            has_beginning = element
        elif column_name == "StudyEndDate":
            has_end = element
        elif column_name == "City":
            city = element
        elif column_name == "Country":
            country = element
        elif column_name == "methods":
            orkg.statements.add(subject_id=contribution_id, predicate_id=property_ids[column_name], object_id=resource_ids[element])
        elif column_name == "platform":
            if element == "both":
                orkg.statements.add(subject_id=contribution_id, predicate_id=property_ids["measurement platform"], object_id=resource_ids["Ground-based"])
                orkg.statements.add(subject_id=contribution_id, predicate_id=property_ids["measurement platform"], object_id=resource_ids["Satellites"])
            else:
                orkg.statements.add(subject_id=contribution_id, predicate_id=property_ids["measurement platform"], object_id=resource_ids[element])
        elif column_name == "StringencyIndex":
            literal = orkg.literals.add(label=element)
            literal_id = literal.content["id"]
            orkg.statements.add(subject_id=contribution_id, predicate_id=property_ids[column_name], object_id=literal_id)
        elif "prcnt_change" in column_name and "prcnt_change_er" not in column_name:
            if not relative_values:
                relative_values = True
                relative_change = orkg.resources.add(label="relative change in air pollution")
                relative_id = relative_change.content["id"]
            add_measurement_statement(orkg, column_name, element, relative_id)
        elif "Reference_avg" in column_name:
            if not reference_values:
                reference_values = True
                reference_measure = orkg.resources.add(label="reference measurements")
                reference_id = reference_measure.content["id"]
            add_measurement_statement(orkg, column_name, element, reference_id)
        elif "Lockdown_avg" in column_name:
            if not lockdown_values:
                lockdown_values = True
                lockdown_measure = orkg.resources.add(label="lockdown measurements")
                lockdown_id = lockdown_measure.content["id"]
            add_measurement_statement(orkg, column_name, element, lockdown_id)

    # add time interval property
    time_interval = orkg.resources.add(label=f"{str(has_beginning)} - {str(has_end)}")
    time_interval_id = time_interval.content["id"]
    beginning_literal = orkg.literals.add(label=has_beginning)
    beginning_literal_id = beginning_literal.content["id"]
    end_literal = orkg.literals.add(label=has_end)
    end_literal_id = end_literal.content["id"]

    orkg.statements.add(subject_id=time_interval_id, predicate_id=property_ids["has beginning"], object_id=beginning_literal_id)
    orkg.statements.add(subject_id=time_interval_id, predicate_id=property_ids["has end"], object_id=end_literal_id)
    orkg.statements.add(subject_id=contribution_id, predicate_id=property_ids["has time"], object_id=time_interval_id)

    if len(city) > 0 and city != "other":
        add_new_location(orkg, city, country, False, contribution_id)
    else:
        add_new_location(orkg, country, country, True, contribution_id)

    # add measured changes to observation
    if relative_values:
        orkg.statements.add(subject_id=contribution_id, predicate_id=property_ids["has relative change"], object_id=relative_id)
    if reference_values:
        orkg.statements.add(subject_id=contribution_id, predicate_id=property_ids["has specified input"], object_id=reference_id)
    if lockdown_values:
        orkg.statements.add(subject_id=contribution_id, predicate_id=property_ids["has specified output"], object_id=lockdown_id)

    print("created new contribution")
    return


def add_new_location(orkg, label, country, only_country, contribution_id):
    """
    This function adds a location to the given contribution.
    If the location is not yet present in the orkg, we execute a sparql query to retrieve the wikidata id for the
    location.
    :param orkg: orkg client
    :param label: name of the location
    :param country: name of the country
    :param only_country: if true, we only look for the country (no city specified)
    :param contribution_id: the id of the contribution where we add the location
    :return:
    """
    # check if city already known
    if label not in location_wikidata_ids.keys():

        # create new resource and link it to wikidata entity
        label_id = sparql_query(label, only_country)

        # try the country if the city could not be found on wikidata
        if not label_id and not only_country:
            if country in location_wikidata_ids.keys():
                location_wikidata_ids[label] = location_wikidata_ids[country]
                label_id = "temp"
            else:
                label_id = sparql_query(country, True)

        # if even the country could not be found then just return
        if not label_id:
            return

        # create a new wikidata reference resource if not already existing
        if label_id != "temp":
            location_wikidata_ids[label] = label_id

            # check if resource already existing
            res = orkg.resources.by_id(id=label_id)

            if type(res.content) == bytes:
                orkg.resources.add(id=label_id, label=label)
                literal = orkg.literals.add(label=f"https://www.wikidata.org/entity/{label_id.split(':')[1]}")
                orkg.statements.add(subject_id=label_id, predicate_id="SAME_AS", object_id=literal.content["id"])

    label_id = location_wikidata_ids[label]
    orkg.statements.add(subject_id=contribution_id, predicate_id=property_ids["occurs in"], object_id=label_id)


def add_measurement_statement(orkg, column_name, element, subject_id):
    """
    This function adds a measurement statement to a given subject.
    :param orkg: orkg client
    :param column_name: column name
    :param element: value of the pollutant
    :param subject_id: id of subject we want to add a measurement to
    :return:
    """
    literal = orkg.literals.add(label=element)
    literal_id = literal.content["id"]

    split_name = column_name.split("_")
    pollutant = split_name[0]
    unit = split_name[1]

    # some measurements don't have a unit such as AOD and AQI
    if unit not in resource_ids.keys():
        unit = None

    new_pollutant = orkg.resources.add(label=pollutant)
    new_pollutant_id = new_pollutant.content["id"]

    if unit:
        orkg.statements.add(subject_id=new_pollutant_id, predicate_id=property_ids["has unit"], object_id=resource_ids[unit])

    orkg.statements.add(subject_id=new_pollutant_id, predicate_id=property_ids["numeric value"], object_id=literal_id)
    orkg.statements.add(subject_id=subject_id, predicate_id=property_ids["has component"], object_id=new_pollutant_id)
    return


def sparql_query(label, only_country):
    """
    This function executes a sparql query to retrieve wikidata ids for cities and countries.
    :param label: the name of the object that should be queried
    :param only_country: if true, we only query for countries (not for cities)
    :return: wikidata id for the given label
    """
    query = """
    SELECT ?item ?itemLabel WHERE {
        ?item wdt:P31 wd:%s.
        ?item rdfs:label ?itemLabel.
    }
    """
    if only_country:
        # query for all countries on wikidata
        final_query = query % "Q6256"
    else:
        # query for all big cities on wikidata
        final_query = query % "Q1549591"
    try:
        res = return_sparql_query_results(final_query)
    except:
        print("too many requests... wait a minute")
        time.sleep(60)
        res = return_sparql_query_results(final_query)

    for binding in res["results"]["bindings"]:
        item_label = binding["itemLabel"]["value"]
        if item_label == label:
            return split_response_item(binding["item"]["value"])

    for binding in res["results"]["bindings"]:
        item_label = binding["itemLabel"]["value"]
        if label in item_label:
            return split_response_item(binding["item"]["value"])

    if not only_country:
        # maybe it is not a big city but a small one
        final_query = query % "Q515"

    try:
        res = return_sparql_query_results(final_query)
    except:
        print("too many requests... wait a minute")
        time.sleep(60)
        res = return_sparql_query_results(final_query)

    for binding in res["results"]["bindings"]:
        item_label = binding["itemLabel"]["value"]
        if item_label == label:
            return split_response_item(binding["item"]["value"])

    for binding in res["results"]["bindings"]:
        item_label = binding["itemLabel"]["value"]
        if label in item_label:
            return split_response_item(binding["item"]["value"])

    return None


def split_response_item(item):
    """
    This function splits a wikidata URI into a wikidata id usable in the orkg.
    :param item: wikidata item that needs to be split
    :return: wikidata id
    """
    item_split = item.split("/")
    return "wikidata:" + item_split[-1]


if __name__ == "__main__":

    # TODO: input valid credentials
    EMAIL = "example@gmail.com"
    PASSWORD = "password123"

    # switch host to https://orkg.org/" for live server
    orkg_client = ORKG(host="https://sandbox.orkg.org/", creds=(EMAIL, PASSWORD))

    # TODO: specify input filename in the global variables
    # run this function once to add all needed predicates and resources to the ORKG
    add_predicates_to_orkg(orkg_client)
    add_resources_to_orkg(orkg_client)

    # get the predicate_ids and resource_ids for the corresponding predicate/resource names
    get_predicate_ids()
    get_resource_ids()

    # run the upload pipeline
    new_paper_upload_function(orkg_client)