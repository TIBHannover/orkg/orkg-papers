## Data import process

_Disclaimer:_

> This document is curated based on the current state of ORKG implementation (date: 16.01.2023) and it serves as an example and a set of guidelines or a checklist for importing data to ORKG. It is important to keep in mind that each data set may come in a different format, therefore, this document aims to cover all the necessary points that you need to consider when importing data, along with some recommendations for the best strategy to import the data. So it is not a one-size-fits-all solution, and you may need to adapt the guidelines to suit the specific needs of your data set.

In this document, the term "entity" refers to any of the following: resource, property, class, or literal.

**Some useful snippets are in this [Jupyter notebook](https://gitlab.com/TIBHannover/orkg/orkg-papers/-/blob/master/import-process/Data%20import%20process.ipynb)**

Before importing any dataset let’s have a quick overview on the orkg data model and how the data is stored in the graph.

### **Data model**

In ORKG, resources are described using a model similar to RDF. These resources have properties, which are characteristics or attributes of the resource. A resource can be any object that can be further described and it’s identified by an _ID_. The properties of a resource are identified through statements, which express the relationship between the resource and a specific value. This value can be either literal, such as text strings or numbers, or it can be another resource that has its own properties. The ORKG statement is illustrated in Figure 1.

![Figure 1](./img/Figure1.png)

Figure 1

To give an example of a statement let’s have a look at its JSON serialization given by the [ORKG Backend API](http://tibhannover.gitlab.io/orkg/orkg-backend/api-doc/) in Figure 2.

![Figure 2](./img/Figure2.png)

Figure 2: Open the image in a separate tab to improve the readability.

As you can see a statement has a subject, predicate and an object and each one of those has its identifier (_ID_). In this example, a paper resource with an id R281017 has a publication year 2012. Notice that the subject resource has classes = ['Paper'] and the **\_class** attribute is 'resource' although the object in this case is just a literal with an id 'L617021' and the **\_class** attribute is 'literal'. The rest of the attributes are just metadata for the provenance of data, like who added this statement or resource (created_by), when (created_at) and whether it's a featured content or not.

Keep in mind that we assign an auto incremented id for each newly added entity (resource,property…), so the id that you will have for a paper in the testing environment is unlikely to have the same id on the public instance.

### **Use a testing server**

It is not recommended to import data directly to the public instance [orkg.org](http://orkg.org/); instead, we suggest using a testing instance on your local machine, [incubating.orkg.org](http://incubating.orkg.org/) or [sandbox.orkg.org](http://sandbox.orkg.org/), for testing purposes.

If you want to run ORKG in your machine, please have a look at the Readme file of the [backend](https://gitlab.com/TIBHannover/orkg/orkg-backend) and [frontend](https://gitlab.com/TIBHannover/orkg/orkg-frontend) repositories.

### **Tools**

In the attached snippets we are going to use python programming language, and ORKG already has a [python package](https://pypi.org/project/orkg/) that provides easy access to the API.

Jupyter notebook and Pandas packages can be very helpful as well to analyze, manipulate and verify the data before processing to the import.

### **Analyze your data**

If you plan to import data into ORKG, it is likely that your data will include papers and associated metadata, such as authors, publication year, and DOI. However, in ORKG, the focus is on the contribution descriptions, such as the research problem addressed, materials used, methods employed, and results obtained, rather than on the metadata.

It is essential to clarify the distinction between paper metadata and the actual contribution data within your data. In the ORKG ontology, the latter is represented as its own resource and is classified as a **Contribution**. As shown in Figure 3, the paper resource is linked to both the meta-information and the contribution resources.

![Figure 3](./img/Figure3.png)

Figure 3

### **How to prepare the meta information?**

For the meta information, the most important fields are:

- Title
- DOI or the URL of the paper
- Authors with their [ORCID](https://orcid.org/) identifiers
- ORKG Research field id

You can find the Research field id for your papers in the [Research fields taxonomy](https://orkg.org/fields) and you choose the most suitable field(s) for your papers. Once you have identified the appropriate field(s), you can begin preparing the papers for import by creating Python objects with the following structure:

![Figure 4](./img/Figure4.png)

Figure 4

You can use some external resources to complete this metadata. Like [Crossref API](https://github.com/sckott/habanero), [semantic scholar API](https://github.com/danielnsilva/semanticscholar). You can store the relevant Python objects in a file using cPickle, which can then be used during the import process.

### **How to model the contribution data?**

We don’t give a strict answer for this question, you are relatively free on how to model your data. Please have a look at some ORKG paper descriptions and check the list of [best practices](https://orkg.org/help-center/article/42/Modeling_best_practices_for_resources_and_properties) to label resources and predicates.

### **Consider the copyright**

Please be aware that the content you import into ORKG may contain copyrighted material and information that is not intended for public distribution. All data imported into ORKG will be published by TIB under the [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

### **Consider having checkpoints**

When importing data, it is important to consider implementing checking-points, so that if errors occur or your script stops running, you will be able to retrieve the data from the point at which it stopped, allowing you to easily resume the import process.

If the import process is taking so long, consider the usage of [Google colab](https://colab.research.google.com/).

### **Use a specific user**

We recommend using a specific user to import the data, as it will serve as an identifier for the import process and can be used to easily rollback in case of errors.

It is also possible to use multiple ORKG accounts if your data has been curated by multiple individuals and you wish to give credit to each of them. To use these accounts, you will need to have their credentials (email, password).

### **Mapping the entities**

The first step on the actual import is to find the mapping between the entities of your data and the ORKG equivalent. As an example you can create a table with the following columns:

- **Label**: The label of the entity
- **IRI**: If your data already has URIs for the entities it would be better to store them in ORKG as well and you can use this table column to find the equivalent ID in ORKG while parsing the data.
- **Ignore**: Sometimes you have to ignore some entities from data due to irrelevance or copyright. Set the value of this column to false by default and set it to true manually if you want to ignore this entry during the import.
- **ORKG_ID**: The entity ID in ORKG.
- **IsStatementsCreated**: This flag plays a checkpoint role for the row.
- **Comment**: Human readable comment that you can use to explain some decision like why you ignored the row…

Please check the jupyter notebook file to see an example.

### **How to check if an entity already exist in ORKG**

In ORKG, there are many properties and classes that have already been created, and we encourage users to re-use these existing properties and classes when importing data. However, it is important to note that each paper has its own unique definition for certain resources and as such, it is recommended to avoid reusing resources. Additionally, your data can reuse resources that you created during the import process.

The ORKG API provides endpoints for looking up properties, classes, or resources by label. If you cannot find an entity that matches the label of your entity, you can create a new one. The notebook provided includes example functions to help with finding or creating entities.

### **Import Strategy**

Before you import the papers, you need first to import all the entities that you papers use in the contribution description. So after mapping and importing all the properties and classes, you start importing resources without the papers, then you start importing the description of all the imported resources by adding statements. we recommend importing the entities in the following order:

1. Properties
2. Classes
3. Resources except papers
4. Statement of resources except paper resources

For the first 3 steps consider the creation of the same as statement if your data provides URIs for each entity.

### **Importing papers**

As a final step, you import the papers by using add paper endpoint and use the entities that you created in the previous steps.

### **What should I do if something goes wrong?**

If you encounter any issues, you can always edit, add, or delete data from ORKG using the same API. To do so, please consult the ORKG Backend [API documentation](http://tibhannover.gitlab.io/orkg/orkg-backend/api-doc/) and use the checkpoint files to access the nodes that contain errors and make corrections.

### **Useful links**

- [Backend repository](https://gitlab.com/TIBHannover/orkg/orkg-backend)
- [Frontend repository](https://gitlab.com/TIBHannover/orkg/orkg-frontend)
- [ORKG API documentations](http://tibhannover.gitlab.io/orkg/orkg-backend/api-doc/)
- [ORKG Python package documentation](https://orkg.readthedocs.io/en/latest/introduction.html)
- [Google colab](https://colab.research.google.com/)
- [Useful snippets](https://gitlab.com/TIBHannover/orkg/orkg-papers/-/blob/master/import-process/Data%20import%20process.ipynb)
