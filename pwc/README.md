# Importing Paper to from Paper with code to ORKG
You can use the notebook in the notebook folder to download and upload the data into the ORKG

## Data Download 
- `bash download_pwc_json.sh`

## Register a user (for import)

```shell
curl -i -X POST -H 'Content-Type: application/json' -d '{"email":"paperwithcode@gmail.com","password":"paperwithcode","matching_password":"paperwithcode","display_name":"Papers With Code"}' http://localhost:8080/api/auth/register
```

## Main Python file for data import 
You need to make sure that you have created a username and password that you need for the next step. 

- `python import_pwc_json_to_orkg.py --path_evaluation_tables ./data/evaluation-tables.json --path_paper_with_abstracts ./data/papers-with-abstracts.json --email paperwithcode@gmail.com --password paperwithcode --api http://localhost:8000`

To run with the filter paper tile:
- `python import_pwc_json_to_orkg.py --path_evaluation_tables ./data/evaluation-tables.json --path_paper_with_abstracts ./data/papers-with-abstracts.json --email paperwithcode@gmail.com --password paperwithcode --api http://localhost:8000 --path_title_list ./data/Paper_titles_list.txt`

## Known issues and limitation

- The code currently only processes content from arxiv and relies on that when constructing URLs to the paper.
- When using the option to filter for only specific titles, all required resources will be created nevertheless, so "adding" papers later means deleting and re-importing due to duplication.
