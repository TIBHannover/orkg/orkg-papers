#!/usr/bin/env bash

# Download Papers with abstract file 
wget -N https://paperswithcode.com/media/about/papers-with-abstracts.json.gz -P data/ 
gzip -d data/papers-with-abstracts.json.gz -f

# Download Evaluation tables file 
wget -N https://paperswithcode.com/media/about/evaluation-tables.json.gz -P data/ 
gzip -d data/evaluation-tables.json.gz -f
