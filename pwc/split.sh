#!/usr/bin/env sh

if ! command -v jq >/dev/null; then
  echo "ERROR: jq not found! Please install it before running this script!"
  exit 1
fi

eval_table="data/evaluation-tables.json"
#paper_abstract="data/papers-with-abstracts.json"

eval_table_total=$(jq '. | length' <"$eval_table")
#paper_abstract_total=$(jq '. | length' < "$paper_abstract")

echo "Number of objects in the evaluation table: $eval_table_total"
#echo "Number of papers with abstracts:           $paper_abstract_total"

# Split evaluation into non-overlapping parts
printf "Splitting..."
from=0
for elements in 1 10 100 ""; do
  if [ -n "$elements" ]; then
    to=$((from + elements))
    suffix=$elements
  else
    to=""
    suffix="rest"
  fi
  jq ".[${from}:${to}]" <"$eval_table" >"data/eval_parts_${suffix}.json"
  from=$to
done
echo " done."
