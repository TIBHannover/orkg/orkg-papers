import requests
from orkg import ORKG
from habanero import Crossref
from shortid import ShortId

orkg = ORKG()
vocab = dict()
sid = ShortId()
cr = Crossref()

'''

Data from: 
Diefenbach, Dennis, et al. "Core techniques of question answering systems over knowledge bases: a survey." 
Knowledge and Information systems 55.3 (2018): 529-569.

'''

def main():
    ############################# PROPERTIES AND RESOURCES #############################

    implementationPredicate = createOrFindPredicate('Implementation')
    evaluationPredicate = createOrFindPredicate('Evaluation')
    datasetPredicate = createOrFindPredicate('Dataset')
    taskPredicate = createOrFindPredicate('Task')
    testQuestionsPredicate = createOrFindPredicate('Test questions')
    trainQuestionsPredicate = createOrFindPredicate('Train questions')
    questionLanguagePredicate = createOrFindPredicate('Question language')
    languagePredicate = createOrFindPredicate('Language')
    questionAmountPredicate = createOrFindPredicate('Question amount')
    precisionPredicate = createOrFindPredicate('Precision')
    recallPredicate = createOrFindPredicate('Recall')
    fmeasurePredicate = createOrFindPredicate('F-measure')
    runtimePredicate = createOrFindPredicate('Runtime')
    onPredicate = createOrFindPredicate('On')
    techniquePredicate = createOrFindPredicate('Technique')

    questionAnalysisTaskPredicate = createOrFindPredicate('Question analysis task')
    phraseMappingTaskPredicate = createOrFindPredicate('Phrase mapping task')
    disambiguationTaskPredicate = createOrFindPredicate('Disambiguation task')
    queryContructionTaskPredicate = createOrFindPredicate('Query construction task')

    # question analysis task
    questionAnalysisNER = orkg.resources.add(label='NER').content['id']
    questionAnalysisNENGramStrategy = orkg.resources.add(label='NE n-gram strategy').content['id']
    questionAnalysisELTools = orkg.resources.add(label='EL tools').content['id']
    questionAnalysisPOSHandmade = orkg.resources.add(label='POS handmade').content['id']
    questionAnalysisPOSLearned = orkg.resources.add(label='POS learned').content['id']
    questionAnalysisParserStructuralGrammar = orkg.resources.add(label='Parser structural grammar').content['id']
    questionAnalysisDependencyParser = orkg.resources.add(label='Dependency parser').content['id']
    questionAnalysisPhraseDependenciesAndDAG = orkg.resources.add(label='Phrase dependencies and DAG').content['id']

    # phrase mapping task
    phraseMappingTaskKnowledgeBaseLabels = orkg.resources.add(label='Knowledge base labels').content['id']
    phraseMappingTaskStringSimilarity = orkg.resources.add(label='String similarity').content['id']
    phraseMappingTaskLuceneIndexOrSimilar = orkg.resources.add(label='Lucene index or similar').content['id']
    phraseMappingTaskWordNetWiktionary = orkg.resources.add(label='WordNet/Wiktionary').content['id']
    phraseMappingTaskRedirects = orkg.resources.add(label='Redirects').content['id']
    phraseMappingTaskPATTY = orkg.resources.add(label='PATTY').content['id']
    phraseMappingTaskUsingExtractedKnowledge = orkg.resources.add(label='Using extracted knowledge').content['id']
    phraseMappingTaskBOAOrSimilar = orkg.resources.add(label='BOA or similar').content['id']
    phraseMappingTaskDistributionalSemantics = orkg.resources.add(label='Distributional Semantics').content['id']
    phraseMappingTaskWikipediaSpecificApproaches = orkg.resources.add(label='Wikipedia specific approaches').content['id']

    # disambiguation task
    disambiguationTaskLocalDisambiguation = orkg.resources.add(label='Local disambiguation').content['id']
    disambiguationTaskGraphSearch = orkg.resources.add(label='Graph search').content['id']
    disambiguationTaskHMM = orkg.resources.add(label='HMM').content['id']
    disambiguationTaskLIP = orkg.resources.add(label='LIP').content['id']
    disambiguationTaskMLN = orkg.resources.add(label='MLN').content['id']
    disambiguationTaskStructuredPerceptron = orkg.resources.add(label='Structured perceptron').content['id']
    disambiguationTaskUserFeedback = orkg.resources.add(label='User feedback').content['id']

    # query construction task
    queryContructionTaskUsingTemplates = orkg.resources.add(label='Using templates').content['id']
    queryContructionTaskUsingInfoFromTheQA = orkg.resources.add(label='Using info. from the QA').content['id']
    queryContructionTaskUsingSemanticParsing = orkg.resources.add(label='Using Semantic Parsing').content['id']
    queryContructionTaskUsingMachineLearning = orkg.resources.add(label='Using machine learning').content['id']
    queryContructionTaskSemanticInformation = orkg.resources.add(label='Semantic information').content['id']
    queryContructionTaskNotGeneratingSPARQL = orkg.resources.add(label='Not generating SPARQL').content['id']
    
    languageEnglish = orkg.resources.add(label='English').content['id']
    languageFarsi = orkg.resources.add(label='Farsi').content['id']
    languageSpanish = orkg.resources.add(label='Spanish').content['id']
    languageGerman = orkg.resources.add(label='German').content['id']
    languageItalian = orkg.resources.add(label='Italian').content['id']
    languageFrench = orkg.resources.add(label='French').content['id']
    languageDutch = orkg.resources.add(label='Dutch').content['id']
    languageRomanian = orkg.resources.add(label='Romanian').content['id']

    questionAnsweringProblem = orkg.resources.add(label='Question answering systems').content['id']
    questionAnsweringEvaluationProblem = orkg.resources.add(label='Question answering systems evaluation').content['id']

    #QALD1
    QALD1 = orkg.resources.add(label='QALD-1').content['id']
    QALD1_T1 = orkg.resources.add(label='Task 1').content['id']
    QALD1_T2 = orkg.resources.add(label='Task 2').content['id']
    DBpedia36 = orkg.resources.add(label='DBpedia 3.6').content['id']
    MusicBrainz = orkg.resources.add(label='MusicBrainz').content['id']

    #QALD2
    QALD2 = orkg.resources.add(label='QALD-2').content['id']
    QALD2_T1 = orkg.resources.add(label='Task 1').content['id']
    QALD2_T2 = orkg.resources.add(label='Task 2').content['id']
    DBpedia37 = orkg.resources.add(label='DBpedia 3.7').content['id']

    #QALD3
    QALD3 = orkg.resources.add(label='QALD-3').content['id']
    QALD3_T1 = orkg.resources.add(label='Task 1').content['id']
    QALD3_T2 = orkg.resources.add(label='Task 2').content['id']
    DBpedia38 = orkg.resources.add(label='DBpedia 3.8').content['id']

    #QALD4
    QALD4 = orkg.resources.add(label='QALD-4').content['id']
    QALD4_T1 = orkg.resources.add(label='Task 1').content['id']
    QALD4_T2 = orkg.resources.add(label='Task 2').content['id']
    QALD4_T3 = orkg.resources.add(label='Task 3').content['id']
    DBpedia39 = orkg.resources.add(label='DBpedia 3.9').content['id']
    sider = orkg.resources.add(label='SIDER').content['id']
    diseasome = orkg.resources.add(label='Diseasome').content['id']
    drugbank = orkg.resources.add(label='Drugbank').content['id']
    DBpedia39WithAbstracts = orkg.resources.add(label='DBpedia 3.9 with abstracts').content['id']

    #QALD5
    QALD5 = orkg.resources.add(label='QALD-5').content['id']
    QALD5_T1 = orkg.resources.add(label='Task 1').content['id']
    QALD5_T2 = orkg.resources.add(label='Task 2').content['id']
    DBpedia2014 = orkg.resources.add(label='DBpedia 2014').content['id']
    DBpedia2014WithAbstracts = orkg.resources.add(label='DBpedia 2014 with abstracts').content['id']

    #QALD6
    QALD6 = orkg.resources.add(label='QALD-6').content['id']
    QALD6_T1 = orkg.resources.add(label='Task 1').content['id']
    QALD6_T2 = orkg.resources.add(label='Task 2').content['id']
    QALD6_T3 = orkg.resources.add(label='Task 3').content['id']
    DBpedia2015 = orkg.resources.add(label='DBpedia 2015').content['id']
    DBpedia2015WithAbstracts = orkg.resources.add(label='DBpedia 2015 with abstracts').content['id']
    LinkedSpending = orkg.resources.add(label='LinkedSpending').content['id']

    techniqueParserStructuralGrammar = orkg.resources.add(label='Parser Structural Grammar').content['id']
    techniqueKnowledgebaseLabels = orkg.resources.add(label='Knowledge base labels').content['id']

    literal50 = orkg.literals.add(label='50').content['id']
    literal100 = orkg.literals.add(label='100').content['id']
    literal99 = orkg.literals.add(label='99').content['id']
    literal25 = orkg.literals.add(label='25').content['id']
    literal10 = orkg.literals.add(label='10').content['id']
    literal350 = orkg.literals.add(label='350').content['id']
    literal200 = orkg.literals.add(label='200').content['id']
    literal300 = orkg.literals.add(label='300').content['id']

    #QALD1
    orkg.statements.add(subject_id=QALD1, predicate_id=taskPredicate, object_id=QALD1_T1)
    orkg.statements.add(subject_id=QALD1, predicate_id=taskPredicate, object_id=QALD1_T2)

    orkg.statements.add(subject_id=QALD1_T1, predicate_id=datasetPredicate, object_id=DBpedia36)
    orkg.statements.add(subject_id=QALD1_T1, predicate_id=testQuestionsPredicate, object_id=literal50)
    orkg.statements.add(subject_id=QALD1_T1, predicate_id=trainQuestionsPredicate, object_id=literal50)
    orkg.statements.add(subject_id=QALD1_T1, predicate_id=questionLanguagePredicate, object_id=languageEnglish)

    orkg.statements.add(subject_id=QALD1_T2, predicate_id=datasetPredicate, object_id=MusicBrainz)
    orkg.statements.add(subject_id=QALD1_T2, predicate_id=testQuestionsPredicate, object_id=literal50)
    orkg.statements.add(subject_id=QALD1_T2, predicate_id=trainQuestionsPredicate, object_id=literal50)
    orkg.statements.add(subject_id=QALD1_T2, predicate_id=questionLanguagePredicate, object_id=languageEnglish)

    #QALD2
    orkg.statements.add(subject_id=QALD2, predicate_id=taskPredicate, object_id=QALD2_T1)
    orkg.statements.add(subject_id=QALD2, predicate_id=taskPredicate, object_id=QALD2_T2)

    orkg.statements.add(subject_id=QALD2_T1, predicate_id=datasetPredicate, object_id=DBpedia37)
    orkg.statements.add(subject_id=QALD2_T1, predicate_id=testQuestionsPredicate, object_id=literal100)
    orkg.statements.add(subject_id=QALD2_T1, predicate_id=trainQuestionsPredicate, object_id=literal100)
    orkg.statements.add(subject_id=QALD2_T1, predicate_id=questionLanguagePredicate, object_id=languageEnglish)

    orkg.statements.add(subject_id=QALD2_T2, predicate_id=datasetPredicate, object_id=MusicBrainz)
    orkg.statements.add(subject_id=QALD2_T2, predicate_id=testQuestionsPredicate, object_id=literal100)
    orkg.statements.add(subject_id=QALD2_T2, predicate_id=trainQuestionsPredicate, object_id=literal100)
    orkg.statements.add(subject_id=QALD2_T2, predicate_id=questionLanguagePredicate, object_id=languageEnglish)

    #QALD3
    orkg.statements.add(subject_id=QALD3, predicate_id=taskPredicate, object_id=QALD3_T1)
    orkg.statements.add(subject_id=QALD3, predicate_id=taskPredicate, object_id=QALD3_T2)

    orkg.statements.add(subject_id=QALD3_T1, predicate_id=datasetPredicate, object_id=DBpedia38)
    orkg.statements.add(subject_id=QALD3_T1, predicate_id=testQuestionsPredicate, object_id=literal99)
    orkg.statements.add(subject_id=QALD3_T1, predicate_id=trainQuestionsPredicate, object_id=literal100)
    orkg.statements.add(subject_id=QALD3_T1, predicate_id=questionLanguagePredicate, object_id=languageEnglish)
    orkg.statements.add(subject_id=QALD3_T1, predicate_id=questionLanguagePredicate, object_id=languageGerman)
    orkg.statements.add(subject_id=QALD3_T1, predicate_id=questionLanguagePredicate, object_id=languageSpanish)
    orkg.statements.add(subject_id=QALD3_T1, predicate_id=questionLanguagePredicate, object_id=languageItalian)
    orkg.statements.add(subject_id=QALD3_T1, predicate_id=questionLanguagePredicate, object_id=languageFrench)
    orkg.statements.add(subject_id=QALD3_T1, predicate_id=questionLanguagePredicate, object_id=languageDutch)

    orkg.statements.add(subject_id=QALD3_T2, predicate_id=datasetPredicate, object_id=MusicBrainz)
    orkg.statements.add(subject_id=QALD3_T2, predicate_id=testQuestionsPredicate, object_id=literal99)
    orkg.statements.add(subject_id=QALD3_T2, predicate_id=trainQuestionsPredicate, object_id=literal100)
    orkg.statements.add(subject_id=QALD3_T2, predicate_id=questionLanguagePredicate, object_id=languageEnglish)

    #QALD4
    orkg.statements.add(subject_id=QALD4, predicate_id=taskPredicate, object_id=QALD4_T1)
    orkg.statements.add(subject_id=QALD4, predicate_id=taskPredicate, object_id=QALD4_T2)
    orkg.statements.add(subject_id=QALD4, predicate_id=taskPredicate, object_id=QALD4_T3)

    orkg.statements.add(subject_id=QALD4_T1, predicate_id=datasetPredicate, object_id=DBpedia39)
    orkg.statements.add(subject_id=QALD4_T1, predicate_id=testQuestionsPredicate, object_id=literal50)
    orkg.statements.add(subject_id=QALD4_T1, predicate_id=trainQuestionsPredicate, object_id=literal200)
    orkg.statements.add(subject_id=QALD4_T1, predicate_id=questionLanguagePredicate, object_id=languageEnglish)
    orkg.statements.add(subject_id=QALD4_T1, predicate_id=questionLanguagePredicate, object_id=languageGerman)
    orkg.statements.add(subject_id=QALD4_T1, predicate_id=questionLanguagePredicate, object_id=languageSpanish)
    orkg.statements.add(subject_id=QALD4_T1, predicate_id=questionLanguagePredicate, object_id=languageItalian)
    orkg.statements.add(subject_id=QALD4_T1, predicate_id=questionLanguagePredicate, object_id=languageFrench)
    orkg.statements.add(subject_id=QALD4_T1, predicate_id=questionLanguagePredicate, object_id=languageDutch)
    orkg.statements.add(subject_id=QALD4_T1, predicate_id=questionLanguagePredicate, object_id=languageRomanian)

    orkg.statements.add(subject_id=QALD4_T2, predicate_id=datasetPredicate, object_id=sider)
    orkg.statements.add(subject_id=QALD4_T2, predicate_id=datasetPredicate, object_id=diseasome)
    orkg.statements.add(subject_id=QALD4_T2, predicate_id=datasetPredicate, object_id=drugbank)
    orkg.statements.add(subject_id=QALD4_T2, predicate_id=testQuestionsPredicate, object_id=literal50)
    orkg.statements.add(subject_id=QALD4_T2, predicate_id=trainQuestionsPredicate, object_id=literal25)
    orkg.statements.add(subject_id=QALD4_T2, predicate_id=questionLanguagePredicate, object_id=languageEnglish)

    orkg.statements.add(subject_id=QALD4_T3, predicate_id=datasetPredicate, object_id=DBpedia39WithAbstracts)
    orkg.statements.add(subject_id=QALD4_T3, predicate_id=testQuestionsPredicate, object_id=literal10)
    orkg.statements.add(subject_id=QALD4_T3, predicate_id=trainQuestionsPredicate, object_id=literal25)
    orkg.statements.add(subject_id=QALD4_T3, predicate_id=questionLanguagePredicate, object_id=languageEnglish)

    #QALD5
    orkg.statements.add(subject_id=QALD5, predicate_id=taskPredicate, object_id=QALD5_T1)
    orkg.statements.add(subject_id=QALD5, predicate_id=taskPredicate, object_id=QALD5_T2)
    
    orkg.statements.add(subject_id=QALD5_T1, predicate_id=datasetPredicate, object_id=DBpedia2014)
    orkg.statements.add(subject_id=QALD5_T1, predicate_id=testQuestionsPredicate, object_id=literal50)
    orkg.statements.add(subject_id=QALD5_T1, predicate_id=trainQuestionsPredicate, object_id=literal300)
    orkg.statements.add(subject_id=QALD5_T1, predicate_id=questionLanguagePredicate, object_id=languageEnglish)
    orkg.statements.add(subject_id=QALD5_T1, predicate_id=questionLanguagePredicate, object_id=languageGerman)
    orkg.statements.add(subject_id=QALD5_T1, predicate_id=questionLanguagePredicate, object_id=languageSpanish)
    orkg.statements.add(subject_id=QALD5_T1, predicate_id=questionLanguagePredicate, object_id=languageItalian)
    orkg.statements.add(subject_id=QALD5_T1, predicate_id=questionLanguagePredicate, object_id=languageFrench)
    orkg.statements.add(subject_id=QALD5_T1, predicate_id=questionLanguagePredicate, object_id=languageDutch)
    orkg.statements.add(subject_id=QALD5_T1, predicate_id=questionLanguagePredicate, object_id=languageRomanian)

    orkg.statements.add(subject_id=QALD5_T2, predicate_id=datasetPredicate, object_id=DBpedia2014WithAbstracts)
    orkg.statements.add(subject_id=QALD5_T2, predicate_id=testQuestionsPredicate, object_id=literal10)
    orkg.statements.add(subject_id=QALD5_T2, predicate_id=trainQuestionsPredicate, object_id=literal50)
    orkg.statements.add(subject_id=QALD5_T2, predicate_id=questionLanguagePredicate, object_id=languageEnglish)

    #QALD6
    orkg.statements.add(subject_id=QALD6, predicate_id=taskPredicate, object_id=QALD6_T1)
    orkg.statements.add(subject_id=QALD6, predicate_id=taskPredicate, object_id=QALD6_T2)
    orkg.statements.add(subject_id=QALD6, predicate_id=taskPredicate, object_id=QALD6_T3)

    orkg.statements.add(subject_id=QALD6_T1, predicate_id=datasetPredicate, object_id=DBpedia2015)
    orkg.statements.add(subject_id=QALD6_T1, predicate_id=testQuestionsPredicate, object_id=literal100)
    orkg.statements.add(subject_id=QALD6_T1, predicate_id=trainQuestionsPredicate, object_id=literal350)
    orkg.statements.add(subject_id=QALD6_T1, predicate_id=questionLanguagePredicate, object_id=languageEnglish)
    orkg.statements.add(subject_id=QALD6_T1, predicate_id=questionLanguagePredicate, object_id=languageGerman)
    orkg.statements.add(subject_id=QALD6_T1, predicate_id=questionLanguagePredicate, object_id=languageSpanish)
    orkg.statements.add(subject_id=QALD6_T1, predicate_id=questionLanguagePredicate, object_id=languageItalian)
    orkg.statements.add(subject_id=QALD6_T1, predicate_id=questionLanguagePredicate, object_id=languageFrench)
    orkg.statements.add(subject_id=QALD6_T1, predicate_id=questionLanguagePredicate, object_id=languageDutch)
    orkg.statements.add(subject_id=QALD6_T1, predicate_id=questionLanguagePredicate, object_id=languageRomanian)
    orkg.statements.add(subject_id=QALD6_T1, predicate_id=questionLanguagePredicate, object_id=languageFarsi)

    orkg.statements.add(subject_id=QALD6_T2, predicate_id=datasetPredicate, object_id=DBpedia2015WithAbstracts)
    orkg.statements.add(subject_id=QALD6_T2, predicate_id=testQuestionsPredicate, object_id=literal25)
    orkg.statements.add(subject_id=QALD6_T2, predicate_id=trainQuestionsPredicate, object_id=literal50)
    orkg.statements.add(subject_id=QALD6_T2, predicate_id=questionLanguagePredicate, object_id=languageEnglish)

    orkg.statements.add(subject_id=QALD6_T3, predicate_id=datasetPredicate, object_id=LinkedSpending)
    orkg.statements.add(subject_id=QALD6_T3, predicate_id=testQuestionsPredicate, object_id=literal50)
    orkg.statements.add(subject_id=QALD6_T3, predicate_id=trainQuestionsPredicate, object_id=literal100)
    orkg.statements.add(subject_id=QALD6_T3, predicate_id=questionLanguagePredicate, object_id=languageEnglish)
    #sid.generate()

    #dataTypeN = orkg.resources.add(label='Numeric').content['id']


    #userRecommendationPredicate = orkg.predicates.add(label='User recommendation').content['id']

    ############################# PAPER 2 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "A system description of natural language query over dbpedia",
            "authors": [{"label": "Aggarwal N"}, {"label": "Buitelaar P"}], 
            "publicationYear": 2012,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "SemSeK",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        { "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        { "@id": questionAnalysisParserStructuralGrammar },
                                        { "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        { "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        { "@id": phraseMappingTaskWordNetWiktionary },
                                        { "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        { "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        { "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        { "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 26 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "Intui2: a prototype system for question answering over linked data",
            "authors": [{"label": "Dima C"}], 
            "publicationYear": 2013,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Intui2",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        { "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        #{ "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        { "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 27 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "Answering natural language questions with Intui3",
            "authors": [{"label": "Dima C "}], 
            "publicationYear": 2014,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Intui3",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        { "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        { "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        { "@id": phraseMappingTaskWordNetWiktionary },
                                        { "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        { "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        { "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 22 #############################

    paper = {
        "paper": {
            "doi": "10.1007/978-3-642-13486-9_8",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "FREyA",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        { "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        { "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        { "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        { "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        { "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER  #############################

    paper = {
        "paper": {
            "doi": "10.1016/j.websem.2013.05.006",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "FREyA",
                                "values": {
                                    onPredicate: [{"@id": QALD1}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.54"}],
                                    recallPredicate: [{"text": "0.46"}],
                                    fmeasurePredicate: [{"text": "0.50"}],
                                    runtimePredicate: [{"text": "36s"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "PowerAqua",
                                "values": {
                                    onPredicate: [{"@id": QALD1}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.48"}],
                                    recallPredicate: [{"text": "0.44"}],
                                    fmeasurePredicate: [{"text": "0.46"}],
                                    runtimePredicate: [{"text": "20s"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 3",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "SemSeK",
                                "values": {
                                    onPredicate: [{"@id": QALD2}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "100"}],
                                    precisionPredicate: [{"text": "0.35"}],
                                    recallPredicate: [{"text": "0.38"}],
                                    fmeasurePredicate: [{"text": "0.37"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 4",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "QAKiS",
                                "values": {
                                    onPredicate: [{"@id": QALD2}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "100"}],
                                    precisionPredicate: [{"text": "0.14"}],
                                    recallPredicate: [{"text": "0.13"}],
                                    fmeasurePredicate: [{"text": "0.13"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)
    
    ############################# PAPER 70 #############################

    paper = {
        "paper": {
            "doi": "10.1145/2187836.2187923",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Evaluation 1",
                                "values": {
                                    onPredicate: [{"@id": QALD1}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.41"}],
                                    recallPredicate: [{"text": "0.42"}],
                                    fmeasurePredicate: [{"text": "0.42"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                        implementationPredicate: [
                            {
                                "label": "TBSL",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        { "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        { "@id": phraseMappingTaskStringSimilarity },
                                        { "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        { "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        { "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        { "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)
       
    ############################# PAPER 76 #############################

    paper = {
        "paper": {
            "doi": "10.1007/978-3-642-35173-0_25",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Evaluation 1",
                                "values": {
                                    onPredicate: [{"@id": QALD2}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "100"}],
                                    precisionPredicate: [{"text": "0.19"}],
                                    recallPredicate: [{"text": "0.22"}],
                                    fmeasurePredicate: [{"text": "0.21"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                        implementationPredicate: [
                            {
                                "label": "BELA",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        { "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        { "@id": phraseMappingTaskStringSimilarity },
                                        { "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        { "@id": phraseMappingTaskWordNetWiktionary },
                                        { "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        { "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        { "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER 96 #############################

    paper = {
        "paper": {
            "doi": "10.1145/2588555.2610525",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "gAnswer",
                                "values": {
                                    onPredicate: [{"@id": QALD3}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "100"}],
                                    precisionPredicate: [{"text": "0.40"}],
                                    recallPredicate: [{"text": "0.40"}],
                                    fmeasurePredicate: [{"text": "0.40"}],
                                    runtimePredicate: [{"text": "1s"}]
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                        implementationPredicate: [
                            {
                                "label": "gAnswer",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        { "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        { "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        { "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        { "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        { "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        { "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "DEANNA",
                                "values": {
                                    onPredicate: [{"@id": QALD3}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "100"}],
                                    precisionPredicate: [{"text": "0.21"}],
                                    recallPredicate: [{"text": "0.21"}],
                                    fmeasurePredicate: [{"text": "0.21"}],
                                    runtimePredicate: [{"text": "1-50s"}]
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER 57 #############################

    paper = {
        "paper": {
            "doi": "10.3233/SW-160223",
            #"title": "",
            #"authors": [{"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}], 
            #"publicationYear": 0000,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        { "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        { "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 15 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "QAKiS: an open domain QA system based on relational patterns.",
            "authors": [{"label": "Cabrio E"}, {"label": "Cojan J"}, {"label": "Aprosio AP"}, {"label": "Magnini B"}, {"label": "Lavelli A"}, {"label": "Gandon F"}], 
            "publicationYear": 2012,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "QAKiS",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        { "@id": questionAnalysisNER },
                                        { "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        { "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        { "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 16 #############################

    paper = {
        "paper": {
            "doi": "10.1007/978-3-642-40802-1_30",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "RTV",
                                "values": {
                                    onPredicate: [{"@id": QALD3}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "99"}],
                                    precisionPredicate: [{"text": "0.32"}],
                                    recallPredicate: [{"text": "0.34"}],
                                    fmeasurePredicate: [{"text": "0.33"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Intui2",
                                "values": {
                                    onPredicate: [{"@id": QALD3}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "99"}],
                                    precisionPredicate: [{"text": "0.32"}],
                                    recallPredicate: [{"text": "0.32"}],
                                    fmeasurePredicate: [{"text": "0.32"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 3",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "SWIP",
                                "values": {
                                    onPredicate: [{"@id": QALD3}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "99"}],
                                    precisionPredicate: [{"text": "0.16"}],
                                    recallPredicate: [{"text": "0.17"}],
                                    fmeasurePredicate: [{"text": "0.17"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER 35 #############################

    paper = {
        "paper": {
            "doi": "10.1145/2557500.2557534",
            #"title": "",
            #"authors": [{"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}], 
            #"publicationYear": 0000,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Treo",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        { "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        { "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        { "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        { "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        { "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        { "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 39 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "A HMM-based approach to question answering against linked data",
            "authors": [{"label": "Giannone C"}, {"label": "Bellomaria V"}, {"label": "Basili R"}], 
            "publicationYear": 2013,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "RTV",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        { "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        { "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        { "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        { "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        { "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 66 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "QAnswer-enhanced entity matching for question answering over linked data",
            "authors": [{"label": "Ruseti S"}, {"label": "Mirea A"}, {"label": "Rebedea T"}, {"label": "Trausan-Matu S"}], 
            "publicationYear": 0000,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "QAnswer",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        { "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        { "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        { "@id": phraseMappingTaskStringSimilarity },
                                        { "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        { "@id": phraseMappingTaskWordNetWiktionary },
                                        { "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        { "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        { "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 61 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "ISOFT at QALD-4: semantic similarity-based question answering system over linked data",
            "authors": [{"label": "Park S"}, {"label": "Shim H"}, {"label": "Lee GG"}], 
            "publicationYear": 2014,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "ISOFT",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        { "@id": questionAnalysisELTools },
                                        { "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        { "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        { "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        { "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        { "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        { "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 67 #############################

    paper = {
        "paper": {
            "doi": "10.1145/2588555.2610525",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Evaluation 1",
                                "values": {
                                    onPredicate: [{"@id": QALD3}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "100"}],
                                    precisionPredicate: [{"text": "0.32"}],
                                    recallPredicate: [{"text": "0.32"}],
                                    fmeasurePredicate: [{"text": "0.32"}],
                                    runtimePredicate: [{"text": "10-20s"}]
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                        implementationPredicate: [
                            {
                                "label": "SINA",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        { "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        { "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        { "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Evaluation 2",
                                "values": {
                                    onPredicate: [{"@id": QALD4_T2}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "25"}],
                                    precisionPredicate: [{"text": "0.95"}],
                                    recallPredicate: [{"text": "0.90"}],
                                    fmeasurePredicate: [{"text": "0.92"}],
                                    runtimePredicate: [{"text": "4-120s"}]
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)    

    ############################# PAPER 95 #############################

    paper = {
        "paper": {
            "doi": "10.1007/978-3-319-31676-5_16",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Evaluation 1",
                                "values": {
                                    onPredicate: [{"@id": QALD3}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "99"}],
                                    precisionPredicate: [{"text": "0.38"}],
                                    recallPredicate: [{"text": "0.42"}],
                                    fmeasurePredicate: [{"text": "0.38"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                        implementationPredicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        { "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        { "@id": phraseMappingTaskDistributionalSemantics },
                                        { "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        { "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)  

    ############################# PAPER 71 #############################

    paper = {
        "paper": {
            "title": "Question answering over linked data (QALD-4)",
            "authors": [{"label": "Unger C"}, {"label": "Forascu C"}, {"label": "Lopez V"}, {"label": "Ngomo A-CN"}, {"label": "Cabrio E"}, {"label": "Cimiano P"}, {"label": "Walter S"}], 
            "publicationYear": 2014,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Xser",
                                "values": {
                                    onPredicate: [{"@id": QALD4}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.72"}],
                                    recallPredicate: [{"text": "0.71"}],
                                    fmeasurePredicate: [{"text": "0.72"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "gAnswer",
                                "values": {
                                    onPredicate: [{"@id": QALD4}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.37"}],
                                    recallPredicate: [{"text": "0.37"}],
                                    fmeasurePredicate: [{"text": "0.37"}],
                                    runtimePredicate: [{"text": "0.973s"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 3",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "CASIA",
                                "values": {
                                    onPredicate: [{"@id": QALD4}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.32"}],
                                    recallPredicate: [{"text": "0.40"}],
                                    fmeasurePredicate: [{"text": "0.36"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 4",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Intui3",
                                "values": {
                                    onPredicate: [{"@id": QALD4}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.23"}],
                                    recallPredicate: [{"text": "0.25"}],
                                    fmeasurePredicate: [{"text": "0.24"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 5",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "ISOFT",
                                "values": {
                                    onPredicate: [{"@id": QALD4}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.21"}],
                                    recallPredicate: [{"text": "0.26"}],
                                    fmeasurePredicate: [{"text": "0.23"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 6",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "GFMed",
                                "values": {
                                    onPredicate: [{"@id": QALD4_T2}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "25"}],
                                    precisionPredicate: [{"text": "0.99"}],
                                    recallPredicate: [{"text": "1"}],
                                    fmeasurePredicate: [{"text": "0.99"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 7",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "POMELO",
                                "values": {
                                    onPredicate: [{"@id": QALD4_T2}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "25"}],
                                    precisionPredicate: [{"text": "0.87"}],
                                    recallPredicate: [{"text": "0.82"}],
                                    fmeasurePredicate: [{"text": "0.85"}],
                                    runtimePredicate: [{"text": "2s"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER 43 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "Description of the POMELO System for the Task 2 of QALD-2014",
            "authors": [{"label": "Hamon T"}, {"label": "Grabar N"}, {"label": "Mougin F"}, {"label": "Thiessard F"}], 
            "publicationYear": 2014,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "POMELO",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        { "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        { "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)  

    ############################# PAPER 50 #############################

    paper = {
        "paper": {
            "doi": "10.3233/SW-2011-0030",
            #"title": "",
            #"authors": [{"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}], 
            #"publicationYear": 0000,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "PowerAqua",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        { "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        { "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        { "@id": phraseMappingTaskWordNetWiktionary },
                                        { "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        { "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        { "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 42 #############################

    paper = {
        "paper": {
            "doi": "10.1007/978-3-319-19581-0_8",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Evaluation 1",
                                "values": {
                                    onPredicate: [{"@id": QALD4}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.52"}],
                                    recallPredicate: [{"text": "0.13"}],
                                    fmeasurePredicate: [{"text": "0.21"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                        implementationPredicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        { "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        { "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        { "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content) 

    ############################# PAPER 94 #############################

    paper = {
        "paper": {
             "title": "A joint model for question answering over multiple knowledge bases",
            "authors": [{"label": "Zhange Y"}, {"label": "He S"}, {"label": "Liu K"}, {"label": "Zhao J"}], 
            "publicationYear": 2016,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Evaluation 1",
                                "values": {
                                    onPredicate: [{"@id": QALD4_T2}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "25"}],
                                    precisionPredicate: [{"text": "0.89"}],
                                    recallPredicate: [{"text": "0.88"}],
                                    fmeasurePredicate: [{"text": "0.88"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                        implementationPredicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        { "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        { "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        { "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER 63 #############################

    paper = {
        "paper": {
            "doi": "10.1007/978-3-642-29449-5_7",
            #"title": "",
            #"authors": [{"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}], 
            #"publicationYear": 0000,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "SWIP",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        { "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        { "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        { "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        { "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

   ############################# PAPER 68 #############################

    paper = {
        "paper": {
            "doi": "10.1007/978-3-319-25010-6_2",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Evaluation 1",
                                "values": {
                                    onPredicate: [{"@id": QALD4_T2}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "25"}],
                                    precisionPredicate: [{"text": "0.34"}],
                                    recallPredicate: [{"text": "0.80"}],
                                    fmeasurePredicate: [{"text": "0.48"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                        implementationPredicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        { "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER 72 #############################

    paper = {
        "paper": {
            "title": "Answering over linked data (QALD-5)",
            "authors": [{"label": "Unger C"}, {"label": "Forascu C"}, {"label": "Lopez V"}, {"label": "Ngomo A-CN"}, {"label": "Cabrio E"}, {"label": "Cimiano P"}, {"label": "Walter S"}],
            "publicationYear": 2015,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "Xser",
                                "values": {
                                    onPredicate: [{"@id": QALD5}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.74"}],
                                    recallPredicate: [{"text": "0.72"}],
                                    fmeasurePredicate: [{"text": "0.73"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "QAnswer",
                                "values": {
                                    onPredicate: [{"@id": QALD5}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.46"}],
                                    recallPredicate: [{"text": "0.35"}],
                                    fmeasurePredicate: [{"text": "0.40"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 3",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "SemGraphQA",
                                "values": {
                                    onPredicate: [{"@id": QALD5}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.31"}],
                                    recallPredicate: [{"text": "0.32"}],
                                    fmeasurePredicate: [{"text": "0.31"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 4",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "YodaQA",
                                "values": {
                                    onPredicate: [{"@id": QALD5}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "50"}],
                                    precisionPredicate: [{"text": "0.28"}],
                                    recallPredicate: [{"text": "0.25"}],
                                    fmeasurePredicate: [{"text": "0.26"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content) 

    ############################# PAPER 62 #############################

    paper = {
        "paper": {
            "doi": "10.18653/v1/W16-1403",
            #"title": "",
            #"authors": [{"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}, {"label": ""}], 
            #"publicationYear": 0000,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "UTQA",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        { "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        { "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        { "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        { "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 79 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "Xser@ QALD-4: answering natural language questions via phrasal semantic parsing",
            "authors": [{"label": "Xu K"}, {"label": "Feng Y"}, {"label": "Zhao D"}], 
            "publicationYear": 2014,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Xser",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        #{ "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        { "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        #{ "@id": questionAnalysisDependencyParser },
                                        { "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        { "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        { "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        { "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        { "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 73 #############################

    paper = {
        "paper": {
            "doi": "10.1007/978-3-319-46565-4_13",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "UTQA",
                                "values": {
                                    onPredicate: [{"@id": QALD6}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "100"}],
                                    precisionPredicate: [{"text": "0.82"}],
                                    recallPredicate: [{"text": "0.69"}],
                                    fmeasurePredicate: [{"text": "0.75"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "UTQA",
                                "values": {
                                    onPredicate: [{"@id": QALD6}],
                                    languagePredicate: [
                                        {"@id": languageSpanish},
                                    ],
                                    questionAmountPredicate: [{"text": "100"}],
                                    precisionPredicate: [{"text": "0.76"}],
                                    recallPredicate: [{"text": "0.62"}],
                                    fmeasurePredicate: [{"text": "0.68"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 3",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "UTQA",
                                "values": {
                                    onPredicate: [{"@id": QALD6}],
                                    languagePredicate: [
                                        {"@id": languageEnglish},
                                    ],
                                    questionAmountPredicate: [{"text": "100"}],
                                    precisionPredicate: [{"text": "0.70"}],
                                    recallPredicate: [{"text": "0.61"}],
                                    fmeasurePredicate: [{"text": "0.65"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 4",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringEvaluationProblem}
                        ],
                        evaluationPredicate: [
                            {
                                "label": "SemGraphQA",
                                "values": {
                                    onPredicate: [{"@id": QALD6}],
                                    languagePredicate: [
                                        {"@id": languageFarsi},
                                    ],
                                    questionAmountPredicate: [{"text": "100"}],
                                    precisionPredicate: [{"text": "0.70"}],
                                    recallPredicate: [{"text": "0.25"}],
                                    fmeasurePredicate: [{"text": "0.37"}],
                                    # TODO: add the reference to the evaluated system
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER 44 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "CASIA@ V2: a MLN-based question answering system over linked data",
            "authors": [{"label": "He S"}, {"label": "Zhang Y"}, {"label": "Liu K"}, {"label": "Zhao J"}], 
            "publicationYear": 2014,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "CASIA",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        { "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        { "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        { "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        { "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        { "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        { "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        { "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        #{ "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        { "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 8 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "LIMSI participation at QALD-5@CLEF",
            "authors": [{"label": "Beaumont R"}, {"label": "Grau B"}, {"label": "Ligozat A-L"}], 
            "publicationYear": 2015,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "SemGraphQA",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        { "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        #{ "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        { "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        { "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        { "@id": phraseMappingTaskWordNetWiktionary },
                                        { "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        #{ "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        { "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 80 #############################

    paper = {
        "paper": {
            #"doi": "",
            "title": "Natural language questions for the web of data",
            "authors": [{"label": "Yahya M"}, {"label": "Berberich K"}, {"label": "Elbassuoni S"}, {"label": "Ramanath M"}, {"label": "Tresp V"}, {"label": "Weikum G"}], 
            "publicationYear": 2012,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": questionAnsweringProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "DEANNA",
                                "values": {
                                    questionAnalysisTaskPredicate: [
                                        #{ "@id": questionAnalysisNER },
                                        { "@id": questionAnalysisNENGramStrategy },
                                        #{ "@id": questionAnalysisELTools },
                                        { "@id": questionAnalysisPOSHandmade },
                                        #{ "@id": questionAnalysisPOSLearned },
                                        #{ "@id": questionAnalysisParserStructuralGrammar },
                                        { "@id": questionAnalysisDependencyParser },
                                        #{ "@id": questionAnalysisPhraseDependenciesAndDAG }
                                    ],
                                    phraseMappingTaskPredicate: [
                                        { "@id": phraseMappingTaskKnowledgeBaseLabels },
                                        #{ "@id": phraseMappingTaskStringSimilarity },
                                        #{ "@id": phraseMappingTaskLuceneIndexOrSimilar },
                                        #{ "@id": phraseMappingTaskWordNetWiktionary },
                                        #{ "@id": phraseMappingTaskRedirects },
                                        #{ "@id": phraseMappingTaskPATTY },
                                        #{ "@id": phraseMappingTaskUsingExtractedKnowledge },
                                        #{ "@id": phraseMappingTaskBOAOrSimilar },
                                        #{ "@id": phraseMappingTaskDistributionalSemantics },
                                        #{ "@id": phraseMappingTaskWikipediaSpecificApproaches }
                                    ],
                                    disambiguationTaskPredicate: [
                                        { "@id": disambiguationTaskLocalDisambiguation },
                                        #{ "@id": disambiguationTaskGraphSearch },
                                        #{ "@id": disambiguationTaskHMM },
                                        { "@id": disambiguationTaskLIP },
                                        #{ "@id": disambiguationTaskMLN },
                                        #{ "@id": disambiguationTaskStructuredPerceptron },
                                        #{ "@id": disambiguationTaskUserFeedback }
                                    ],
                                    queryContructionTaskPredicate: [
                                        #{ "@id": queryContructionTaskUsingTemplates },
                                        { "@id": queryContructionTaskUsingInfoFromTheQA },
                                        #{ "@id": queryContructionTaskUsingSemanticParsing },
                                        #{ "@id": queryContructionTaskUsingMachineLearning },
                                        #{ "@id": queryContructionTaskSemanticInformation },
                                        #{ "@id": queryContructionTaskNotGeneratingSPARQL }
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    
    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

def doiLookup(paper): 
    if ("doi" in paper['paper']):
        lookupResult = cr.works(ids = paper['paper']['doi'])
        if (lookupResult['status'] == 'ok'):
            print(lookupResult['message']['published-print']['date-parts'][0])
            paper['paper']['title'] = lookupResult['message']['title'][0]
            
            if (len(lookupResult['message']['published-print']['date-parts'][0]) > 0):
                paper['paper']['publicationYear'] = lookupResult['message']['published-print']['date-parts'][0][0]

            if (len(lookupResult['message']['published-print']['date-parts'][0]) > 1):
                paper['paper']['publicationMonth'] = lookupResult['message']['published-print']['date-parts'][0][1]

            if (len(lookupResult['message']['author']) > 0): 
                paper['paper']['authors'] = []
                for author in lookupResult['message']['author']: 
                    paper['paper']['authors'].append({"label": author['given'] + ' ' + author['family']})

    return paper

def createOrFindPredicate(label): 
    findPredicate = orkg.predicates.get(q=label, exact=True).content

    if (len(findPredicate) > 0):
        predicate = findPredicate[0]['id']
    else:
        predicate = orkg.predicates.add(label=label).content['id']

    return predicate

if __name__ == "__main__":
    main()
