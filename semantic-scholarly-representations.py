import requests
from orkg import ORKG
from habanero import Crossref
import json

orkg = ORKG('http://localhost:8000')
vocab = dict()
cr = Crossref()

'''
Importing papers related to semantic representation of scholarly communication 
'''

def main():
    ############################# PROPERTIES AND RESOURCES #############################
    researchProblem = orkg.resources.add(label='Semantic representation of scholarly communication').content['id']

    semanticRepresentation = orkg.predicates.add(label='Semantic representation').content['id']
    scopePredicate = orkg.predicates.add(label='Scope').content['id']
    prospectiveRetrospectivePredicate = orkg.predicates.add(label='Prospective/retrospective').content['id']
    acquisitionPredicate = orkg.predicates.add(label='Acquisition').content['id']
    metadataPredicate = orkg.predicates.add(label='Metadata').content['id']
    highLevelClaimsPredicate = orkg.predicates.add(label='High level claims').content['id']
    supportResearchDataPredicate = orkg.predicates.add(label='Supports research data').content['id']
    naturalLanguageStatementsPredicate = orkg.predicates.add(label='Natural language statements').content['id']
    discoursePredicate = orkg.predicates.add(label='Discourse').content['id']
    dataTypePredicate = orkg.predicates.add(label='Data type').content['id']
    knowledgeRepresentationPredicate = orkg.predicates.add(label='Knowledge representation').content['id']


    ############################# PAPER 1 #############################

    paper = {
        "paper": {
            "doi": "10.1145/3360901.3364435",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "ORKG System",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        semanticRepresentation: [
                            {
                                "label": "ORKG",
                                "values": {
                                    scopePredicate: [{"text": "Summary"}],
                                    prospectiveRetrospectivePredicate: [
                                        {"text": "Prospective"},
                                        {"text": "Retrospective"}
                                    ],
                                    acquisitionPredicate: [
                                        {"text": "Authors"}, 
                                        {"@id": createOrFindResource(label='Crowdsourcing')}
                                    ], 
                                    metadataPredicate: [{"text": "T"}],
                                    highLevelClaimsPredicate: [{"text": "T"}],
                                    supportResearchDataPredicate: [{"text": "Partially"}],
                                    naturalLanguageStatementsPredicate: [{"text": "T"}],
                                    discoursePredicate: [{"text": "T"}],
                                    dataTypePredicate: [{"text": "Free text"}],
                                    knowledgeRepresentationPredicate: [
                                        {"@id": createOrFindResource(label='RDF')},
                                        {"@id": createOrFindResource(label='Metadata')}
                                    ],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 2 #############################

    paper = {
        "paper": {
            "doi": "10.1007/978-3-319-60131-1_33",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        semanticRepresentation: [
                            {
                                "label": "Dokie.li",
                                "values": {
                                    scopePredicate: [{"text": "Full paper"}],
                                    prospectiveRetrospectivePredicate: [
                                        {"text": "Prospective"},
                                    ],
                                    acquisitionPredicate: [
                                        {"text": "Authors"}, 
                                    ], 
                                    metadataPredicate: [{"text": "T"}],
                                    highLevelClaimsPredicate: [{"text": "N"}],
                                    supportResearchDataPredicate: [{"text": "N"}],
                                    naturalLanguageStatementsPredicate: [{"text": "T"}],
                                    discoursePredicate: [{"text": "T"}],
                                    dataTypePredicate: [{"text": "Quoted text"}],
                                    knowledgeRepresentationPredicate: [
                                        {"@id": createOrFindResource(label='HTML')},
                                        {"@id": createOrFindResource(label='RDFa')}
                                    ],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 3 #############################

    paper = {
        "paper": {
            "doi": "10.7717/peerj-cs.132",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        semanticRepresentation: [
                            {
                                "label": "RASH",
                                "values": {
                                    scopePredicate: [{"text": "Full paper"}],
                                    prospectiveRetrospectivePredicate: [
                                        {"text": "Prospective"},
                                    ],
                                    acquisitionPredicate: [
                                        {"text": "Authors"}, 
                                    ], 
                                    metadataPredicate: [{"text": "T"}],
                                    highLevelClaimsPredicate: [{"text": "Partially"}],
                                    supportResearchDataPredicate: [{"text": "No"}],
                                    naturalLanguageStatementsPredicate: [{"text": "T"}],
                                    discoursePredicate: [{"text": "N"}],
                                    dataTypePredicate: [{"text": "Quoted text"}],
                                    knowledgeRepresentationPredicate: [
                                        {"@id": createOrFindResource(label='HTML')},
                                        {"@id": createOrFindResource(label='RDFa')}
                                    ],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 4 #############################

    paper = {
        "paper": {
            "doi": "10.1186/2041-1480-5-28",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        semanticRepresentation: [
                            {
                                "label": "Micropublications",
                                "values": {
                                    scopePredicate: [{"text": "Full paper"}],
                                    prospectiveRetrospectivePredicate: [
                                        {"text": "Prospective"},
                                    ],
                                    acquisitionPredicate: [
                                        {"text": "Authors"}, 
                                    ], 
                                    metadataPredicate: [{"text": "T"}],
                                    highLevelClaimsPredicate: [{"text": "N"}],
                                    supportResearchDataPredicate: [{"text": "T"}],
                                    naturalLanguageStatementsPredicate: [{"text": "T"}],
                                    discoursePredicate: [{"text": "T"}],
                                    dataTypePredicate: [{"text": "Free text"}],
                                    knowledgeRepresentationPredicate: [
                                        {"@id": createOrFindResource(label='RDF')},
                                        {"@id": createOrFindResource(label='OWL')}
                                    ],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 5 #############################

    paper = {
        "paper": {
            "doi": "10.3233/ISU-2010-0613",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        semanticRepresentation: [
                            {
                                "label": "Nanopublications",
                                "values": {
                                    scopePredicate: [{"text": "Statement level"}],
                                    prospectiveRetrospectivePredicate: [
                                        {"text": "Prospective"},
                                        {"text": "Retrospective"}
                                    ],
                                    acquisitionPredicate: [
                                        {"text": "Authors"}, 
                                        {"@id": createOrFindResource(label='Automatic extraction')}
                                    ], 
                                    metadataPredicate: [{"text": "T"}],
                                    highLevelClaimsPredicate: [{"text": "T"}],
                                    supportResearchDataPredicate: [{"text": "T"}],
                                    naturalLanguageStatementsPredicate: [{"text": "N"}],
                                    discoursePredicate: [{"text": "Partially"}],
                                    dataTypePredicate: [{"text": "Free text"}],
                                    knowledgeRepresentationPredicate: [
                                        {"@id": createOrFindResource(label='RDF')},
                                    ],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)


def doiLookup(paper): 
    if ("doi" in paper['paper']):
        lookupResult = cr.works(ids = paper['paper']['doi'])
        if (lookupResult['status'] == 'ok'):
            paper['paper']['title'] = lookupResult['message']['title'][0]
            
            if ('published-print' in lookupResult['message']):
                if (len(lookupResult['message']['published-print']['date-parts'][0]) > 0):
                    paper['paper']['publicationYear'] = lookupResult['message']['published-print']['date-parts'][0][0]

                if (len(lookupResult['message']['published-print']['date-parts'][0]) > 1):
                    paper['paper']['publicationMonth'] = lookupResult['message']['published-print']['date-parts'][0][1]

            elif ('published-online' in lookupResult['message']):
                if (len(lookupResult['message']['published-online']['date-parts'][0]) > 0):
                    paper['paper']['publicationYear'] = lookupResult['message']['published-online']['date-parts'][0][0]

                if (len(lookupResult['message']['published-online']['date-parts'][0]) > 1):
                    paper['paper']['publicationMonth'] = lookupResult['message']['published-online']['date-parts'][0][1]

            
            if (len(lookupResult['message']['author']) > 0): 
                paper['paper']['authors'] = []
                for author in lookupResult['message']['author']: 
                    paper['paper']['authors'].append({"label": author['given'] + ' ' + author['family']})

    return paper


def createOrFindResource(label):
    findResource = orkg.resources.get(q=label, exact=True).content
    if (len(findResource) > 0):
        resource = findResource[0]['id']
    else:
        resource = orkg.resources.add(label=label).content['id']
    return resource



if __name__ == "__main__":
    main()
