import requests
from orkg import ORKG

orkg = ORKG()
vocab = dict()

'''

File to import a survey paper that nicely demonstrates the possibilities of the ORKG comparison feature

Data from: 
Bikakis, N., & Sellis, T. (2016). Exploration and visualization in the web of big 
linked data: A survey of the state of the art. arXiv preprint arXiv:1601.08059.

'''

def main():
    ############################# PROPERTIES AND RESOURCES #############################
    visualizationProblem = orkg.resources.add(label='Generic visualization systems').content['id']

    dataTypeN = orkg.resources.add(label='Numeric').content['id']
    dataTypeT = orkg.resources.add(label='Temporal').content['id']
    dataTypeS = orkg.resources.add(label='Special').content['id']
    dataTypeH = orkg.resources.add(label='Hierarchical (tree)').content['id']
    dataTypeG = orkg.resources.add(label='Graph (network)').content['id']

    visTypeB = orkg.resources.add(label='Bubble chart').content['id']
    visTypeC = orkg.resources.add(label='Chart').content['id']
    visTypeCI = orkg.resources.add(label='Circle').content['id']
    visTypeG = orkg.resources.add(label='Graph').content['id']
    visTypeM = orkg.resources.add(label='Map').content['id']
    visTypeP = orkg.resources.add(label='Pie').content['id']
    visTypePC = orkg.resources.add(label='Parallel coordinates').content['id']
    visTypeS = orkg.resources.add(label='Scatter').content['id']
    visTypeSG = orkg.resources.add(label='Streamgraph').content['id']
    visTypeT = orkg.resources.add(label='Treemap').content['id']
    visTypeTL = orkg.resources.add(label='Timeline').content['id']
    visTypeTR = orkg.resources.add(label='Tree').content['id']

    dataTypePredicate = orkg.predicates.add(label='Data types').content['id']
    visTypePredicate = orkg.predicates.add(label='Visualization types').content['id']

    # find or create implementation
    # TODO: make a generic function for this
    findImplementationPredicate = orkg.predicates.get(q='Implementation', exact=True).content

    if (len(findImplementationPredicate) > 0):
        implementationPredicate = findImplementationPredicate[0]['id']
    else:
        implementationPredicate = orkg.predicates.add(label='Implementation').content['id']

    userRecommendationPredicate = orkg.predicates.add(label='User recommendation').content['id']
    preferencesPredicate = orkg.predicates.add(label='Preferences').content['id']
    statisticsPredicate = orkg.predicates.add(label='Statistics').content['id']
    samplingPredicate = orkg.predicates.add(label='Sampling').content['id']
    aggregationPredicate = orkg.predicates.add(label='Aggregation').content['id']
    incrementalPredicate = orkg.predicates.add(label='Incremental').content['id']
    diskPredicate = orkg.predicates.add(label='Disk').content['id']
    domainPredicate = orkg.predicates.add(label='Domain').content['id']
    applicationTypePredicate = orkg.predicates.add(label='Application type').content['id']

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Facets and Pivoting for Flexible and Usable Linked Data Exploration",
            "authors": [{"label": "Josep Maria Brunetti"}, {"label": "Rosa Gil"}, {"label": "Roberto García"}],
            "publicationMonth": 5,
            "publicationYear": 2012,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Rhizomer",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeN},
                                        {"@id": dataTypeT},
                                        {"@id": dataTypeS},
                                        {"@id": dataTypeH},
                                        {"@id": dataTypeG},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeC},
                                        {"@id": visTypeM},
                                        {"@id": visTypeT},
                                        {"@id": visTypeTL},
                                    ],
                                    userRecommendationPredicate: [{"text": "T"}],
                                    preferencesPredicate: [{"text": "F"}],
                                    statisticsPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Context-aware Recommendation of Visualization Components",
            "authors": [{"label": "Martin Voigt"}, {"label": "Stefan Pietschmann"}, {"label": "Lars Grammel"}, {"label": "Klaus Meißner"}],
            "publicationMonth": 1,
            "publicationYear": 2012,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "VizBoard",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeN},
                                        {"@id": dataTypeH},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeC},
                                        {"@id": visTypeS},
                                        {"@id": visTypeT},
                                    ],
                                    userRecommendationPredicate: [{"text": "T"}],
                                    preferencesPredicate: [{"text": "T"}],
                                    statisticsPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "LODWheel – JavaScript-based Visualization of RDF Data",
            "authors": [{"label": "Magnus Stuhr"}, {"label": "Dumitru Roman"}, {"label": "David Norheim"}],
            "publicationMonth": 1,
            "publicationYear": 2011,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "LODWheel",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeN},
                                        {"@id": dataTypeS},
                                        {"@id": dataTypeG},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeC},
                                        {"@id": visTypeG},
                                        {"@id": visTypeM},
                                        {"@id": visTypeP},
                                    ],
                                    userRecommendationPredicate: [{"text": "F"}],
                                    preferencesPredicate: [{"text": "F"}],
                                    statisticsPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "SemLens: visual analysis of semantic data with scatter plots and semantic lenses",
            "authors": [{"label": "Philipp Heim"}, {"label": "Steffen Lohmann"}, {"label": "Davaadorj Tsendragchaa"}, {"label": "Thomas Ertl"}],
            "publicationMonth": 9,
            "publicationYear": 2011,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "SemLens",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeN},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeS},
                                    ],
                                    userRecommendationPredicate: [{"text": "F"}],
                                    preferencesPredicate: [{"text": "T"}],
                                    statisticsPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Formal Linked Data Visualization Model",
            "authors": [{"label": "Josep Maria Brunetti"}, {"label": "Sören Auer"}, {"label": "Roberto García"}, {"label": "Jakub Klímek"}, {"label": "Martin Necaský"}],
            "publicationMonth": 12,
            "publicationYear": 2013,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "LDVM",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeS},
                                        {"@id": dataTypeH},
                                        {"@id": dataTypeG},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeB},
                                        {"@id": visTypeM},
                                        {"@id": visTypeT},
                                        {"@id": visTypeTR},
                                    ],
                                    userRecommendationPredicate: [{"text": "T"}],
                                    preferencesPredicate: [{"text": "F"}],
                                    statisticsPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Payola: Collaborative Linked Data Analysis and Visualization Framework",
            "authors": [{"label": "Jakub Klímek"}, {"label": "Martin Nečaský"}, {"label": "Martin Nečaský"}],
            "publicationMonth": 5,
            "publicationYear": 2013,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Payola",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeN},
                                        {"@id": dataTypeT},
                                        {"@id": dataTypeS},
                                        {"@id": dataTypeH},
                                        {"@id": dataTypeG},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeC},
                                        {"@id": visTypeCI},
                                        {"@id": visTypeG},
                                        {"@id": visTypeM},
                                        {"@id": visTypeT},
                                        {"@id": visTypeTL},
                                        {"@id": visTypeTR},

                                    ],
                                    userRecommendationPredicate: [{"text": "F"}],
                                    preferencesPredicate: [{"text": "F"}],
                                    statisticsPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Towards a Linked-Data based Visualization Wizard",
            "authors": [{"label": "Ghislain Auguste Atemezing"}, {"label": "Raphaël Troncy"}],
            "publicationMonth": 10,
            "publicationYear": 2014,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "LDVizWiz",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeS},
                                        {"@id": dataTypeH},
                                        {"@id": dataTypeG},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeM},
                                        {"@id": visTypeP},
                                        {"@id": visTypeTR},
                                    ],
                                    userRecommendationPredicate: [{"text": "T"}],
                                    preferencesPredicate: [{"text": "F"}],
                                    statisticsPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "rdf:SynopsViz – A Framework for Hierarchical Linked Data Visual Exploration and Analysis",
            "authors": [{"label": "Nikos Bikakis"}, {"label": "Melina Skourla"}, {"label": "George Papastefanatos"}],
            "publicationMonth": 6,
            "publicationYear": 2014,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "SynopsViz",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeN},
                                        {"@id": dataTypeT},
                                        {"@id": dataTypeH},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeC},
                                        {"@id": visTypeP},
                                        {"@id": visTypeT},
                                        {"@id": visTypeTL},
                                    ],
                                    userRecommendationPredicate: [{"text": "T"}],
                                    preferencesPredicate: [{"text": "T"}],
                                    statisticsPredicate: [{"text": "T"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "T"}],
                                    incrementalPredicate: [{"text": "T"}],
                                    diskPredicate: [{"text": "T"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Using Semantics for Interactive Visual Analysis of Linked Open Data",
            "authors": [{"label": "Gerwald Tschinkel"}, {"label": "Eduardo Veas"}, {"label": "Belgin Mutlu"}, {"label": "Vedran Sabol"}],
            "publicationMonth": 10,
            "publicationYear": 2014,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Vis Wizard",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeN},
                                        {"@id": dataTypeT},
                                        {"@id": dataTypeS},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeB},
                                        {"@id": visTypeC},
                                        {"@id": visTypeM},
                                        {"@id": visTypeP},
                                        {"@id": visTypePC},
                                        {"@id": visTypeSG},
                                    ],
                                    userRecommendationPredicate: [{"text": "T"}],
                                    preferencesPredicate: [{"text": "T"}],
                                    statisticsPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "LinkDaViz – Automatic Binding of Linked Data to Visualizations",
            "authors": [{"label": "Klaudia Thellmann"}, {"label": "Michael Galkin"}, {"label": "Fabrizio Orlandi"}, {"label": "Sören Auer"}],
            "publicationMonth": 10,
            "publicationYear": 2015,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "LinkDaViz",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeN},
                                        {"@id": dataTypeT},
                                        {"@id": dataTypeS},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeB},
                                        {"@id": visTypeC},
                                        {"@id": visTypeS},
                                        {"@id": visTypeM},
                                        {"@id": visTypeP},
                                    ],
                                    userRecommendationPredicate: [{"text": "T"}],
                                    preferencesPredicate: [{"text": "T"}],
                                    statisticsPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Visual analysis of statistical data on maps using linked open data",
            "authors": [{"label": "Petar Ristoski"}, {"label": "Heiko Paulheim"}],
            "publicationMonth": 1,
            "publicationYear": 2016,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": visualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "ViCoMap",
                                "values": {
                                    dataTypePredicate: [
                                        {"@id": dataTypeN},
                                        {"@id": dataTypeT},
                                        {"@id": dataTypeS},
                                    ],
                                    visTypePredicate: [
                                        {"@id": visTypeM},
                                    ],
                                    userRecommendationPredicate: [{"text": "F"}],
                                    preferencesPredicate: [{"text": "F"}],
                                    statisticsPredicate: [{"text": "T"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

if __name__ == "__main__":
    main()
